package ru.devtron.vormeoclient.ui.drawer

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.stepstone.apprating.AppRatingDialog
import kotlinx.android.synthetic.main.fragment_nav_drawer.*
import kotlinx.android.synthetic.main.layout_avatar.*
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.extension.humanYear
import ru.devtron.vormeoclient.extension.loadRoundedImage
import ru.devtron.vormeoclient.model.interactor.profile.MyUserInfo
import ru.devtron.vormeoclient.presentation.drawer.NavigationDrawerPresenter
import ru.devtron.vormeoclient.presentation.drawer.NavigationDrawerView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import toothpick.Toothpick
import java.util.*


class NavigationDrawerFragment : BaseFragment(), NavigationDrawerView {
    override val layoutRes = R.layout.fragment_nav_drawer

    private val itemClickListener = { view: View ->
        presenter.onMenuItemClick(view.tag as NavigationDrawerView.MenuItem)
    }

    @InjectPresenter
    lateinit var presenter: NavigationDrawerPresenter

    @ProvidePresenter
    fun providePresenter(): NavigationDrawerPresenter {
        return Toothpick
                .openScope(DI.DRAWER_FLOW_SCOPE)
                .getInstance(NavigationDrawerPresenter::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        logoutIV.setOnClickListener { _ ->
            alert(R.string.logout_question) {
                yesButton { presenter.onLogoutClick() }
                cancelButton {}
            }.show()
        }

        activityMI.tag = NavigationDrawerView.MenuItem.WATCH_NOW
        profileMI.tag = NavigationDrawerView.MenuItem.PROFILE
        aboutMI.tag = NavigationDrawerView.MenuItem.ABOUT
        feedbackMI.tag = NavigationDrawerView.MenuItem.FEEDBACK

        activityMI.setOnClickListener(itemClickListener)
        profileMI.setOnClickListener(itemClickListener)
        aboutMI.setOnClickListener(itemClickListener)
        feedbackMI.setOnClickListener(itemClickListener)
    }

    override fun showUserInfo(user: MyUserInfo) {
        if (user.user == null) {
            nickTV.text = ""
            sinceTV.text = ""
            letterTV.text = ""
            avatarIV.visibility = View.GONE
        } else with(user.user) {
            nickTV.text = this.name
            val res = view!!.resources
            sinceTV.text = user.user.createdTime.humanYear(res)
            letterTV.text = this.name.first().toString().toUpperCase()
            avatarIV.loadRoundedImage(avatarUrl)
            avatarLay.setOnClickListener { presenter.onUserClick(userId)  }
        }
    }


    override fun selectMenuItem(item: NavigationDrawerView.MenuItem) {
        (0 until navDrawerMenuContainer.childCount)
                .map { navDrawerMenuContainer.getChildAt(it) }
                .forEach { menuItem -> menuItem.tag?.let { menuItem.isSelected = it == item } }
    }

    fun onScreenChanged(item: NavigationDrawerView.MenuItem) {
        presenter.onScreenChanged(item)
    }

    override fun showRatingDialog() {
        AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNeutralButtonText("Later")
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent!"))
                .setDefaultRating(3)
                .setTitle("Rate this application")
                .setDescription("Please select some stars and give your feedback")
                .setStarColor(R.color.ratingColor)
                .setNoteDescriptionTextColor(R.color.grey)
                .setTitleTextColor(R.color.white)
                .setDescriptionTextColor(R.color.grey)
                .setHint("Please write your comment here ...")
                .setHintTextColor(R.color.secondary_text)
                .setCommentTextColor(R.color.white)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)
                .setWindowAnimation(R.style.DialogSlideHorizontalAnimation)
                .create(requireActivity())
                .show()
    }
}