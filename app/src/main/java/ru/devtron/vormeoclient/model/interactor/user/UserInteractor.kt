package ru.devtron.vormeoclient.model.interactor.user

import io.reactivex.Completable
import ru.devtron.vormeoclient.model.repository.user.UserRepository
import javax.inject.Inject

/**
 * Created by ruslanaliev on 11.12.2017.
 */
class UserInteractor @Inject constructor(
        private val userRepository: UserRepository
) {

    fun getUser(id: Long) = userRepository.getUser(id)

    fun getUserVideos(userId: Long, page: Int) = userRepository.getUserVideos(userId = userId, page = page)

    fun followUser(userId: Long, isFollow: Boolean): Completable {
        return if (isFollow) {
            userRepository.unfollowUser(userId)
        } else {
            userRepository.followUser(userId)
        }
    }

}