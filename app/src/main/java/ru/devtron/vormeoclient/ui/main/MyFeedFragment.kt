package ru.devtron.vormeoclient.ui.main

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import kotlinx.android.synthetic.main.fragment_about.*
import kotlinx.android.synthetic.main.layout_base_list.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.entity.feed.FeedElement
import ru.devtron.vormeoclient.extension.showSnackMessage
import ru.devtron.vormeoclient.extension.visible
import ru.devtron.vormeoclient.presentation.main.feed.MyFeedPresenter
import ru.devtron.vormeoclient.presentation.main.feed.MyFeedView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import ru.devtron.vormeoclient.ui.global.list.FeedAdapterDelegate
import ru.devtron.vormeoclient.ui.global.list.ListItem
import ru.devtron.vormeoclient.ui.global.list.ProgressAdapterDelegate
import toothpick.Toothpick

class MyFeedFragment : BaseFragment(), MyFeedView {
    override fun showFeed(show: Boolean, feeds: List<FeedElement>) {
        recyclerView.visible(show)
        recyclerView.post { adapter.setData(feeds) }
    }

    override val layoutRes = R.layout.fragment_my_feed

    private val adapter = FeedsAdapter()

    @InjectPresenter lateinit var presenter: MyFeedPresenter

    @ProvidePresenter
    fun providePresenter(): MyFeedPresenter =
            Toothpick
                    .openScope(DI.DRAWER_FLOW_SCOPE)
                    .getInstance(MyFeedPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = this@MyFeedFragment.adapter
        }

        swipeToRefresh.setOnRefreshListener { presenter.refreshFeeds() }
        toolbar.setNavigationOnClickListener { presenter.onMenuClick() }
    }

    override fun showRefreshProgress(show: Boolean) {
        swipeToRefresh.post { swipeToRefresh.isRefreshing = show }
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView.visible(show)

        //trick for disable and hide swipeToRefresh on fullscreen progress
        swipeToRefresh.visible(!show)
        swipeToRefresh.post { swipeToRefresh.isRefreshing = false }
    }

    override fun showPageProgress(show: Boolean) {
        recyclerView.post { adapter.showProgress(show) }
    }

    override fun showEmptyView(show: Boolean) {
        //todo
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        //todo
        if (show && message != null) showSnackMessage(message)
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    inner class FeedsAdapter : ListDelegationAdapter<MutableList<ListItem>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(FeedAdapterDelegate({ presenter.onFeedClick(it) }, { presenter.profileClick( it) }))
            delegatesManager.addDelegate(ProgressAdapterDelegate())
        }

        fun setData(feeds: List<FeedElement>) {
            val progress = isProgress()

            items.clear()
            items.addAll(feeds.map { ListItem.FeedItem(it) })
            if (progress) items.add(ListItem.ProgressItem())

            notifyDataSetChanged()
        }

        fun showProgress(isVisible: Boolean) {
            val currentProgress = isProgress()

            if (isVisible && !currentProgress) items.add(ListItem.ProgressItem())
            else if (!isVisible && currentProgress) items.remove(items.last())

            notifyDataSetChanged()
        }

        private fun isProgress() = items.isNotEmpty() && items.last() is ListItem.ProgressItem

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any?>) {
            super.onBindViewHolder(holder, position, payloads)
            if (position == items.size - 10) presenter.loadNextFeedsPage()
        }
    }
}