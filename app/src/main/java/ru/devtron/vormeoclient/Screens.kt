package ru.devtron.vormeoclient

import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.entity.user.FollowersHolder
import ru.devtron.vormeoclient.ui.about.AboutFragment
import ru.devtron.vormeoclient.ui.auth.AuthFlowFragment
import ru.devtron.vormeoclient.ui.auth.AuthFragment
import ru.devtron.vormeoclient.ui.drawer.DrawerFlowFragment
import ru.devtron.vormeoclient.ui.global.BaseFragment
import ru.devtron.vormeoclient.ui.likes.LikesFragment
import ru.devtron.vormeoclient.ui.main.MainFlowFragment
import ru.devtron.vormeoclient.ui.main.MyFeedFragment
import ru.devtron.vormeoclient.ui.main.followers.FollowersContainerFragment
import ru.devtron.vormeoclient.ui.main.library.LibraryContainerFragment
import ru.devtron.vormeoclient.ui.user.UserFlowFragment
import ru.devtron.vormeoclient.ui.user.info.UserInfoFragment

object Screens {
    const val DRAWER_FLOW = "drawer flow"

    const val MAIN_FLOW = "main flow"
    const val ABOUT_SCREEN = "about screen"
    const val FEED_SCREEN = "feed screen"
    const val LIBRARY_SCREEN = "library screen"

    const val USER_FLOW = "user flow"
    const val USER_INFO_SCREEN = "user info screen"

    const val FOLLOWERS_SCREEN = "followers screen"
    const val LIKES_SCREEN = "likes screen"
    const val TRENDING_SCREEN = "trending screen"
    const val SUBSCRIPTIONS_SCREEN = "subscriptions screen"

    const val AUTH_FLOW = "auth flow"
    const val AUTH_SCREEN = "oauth screen"

    fun createFragment(screenKey: String, data: Any? = null): BaseFragment? =
            when (screenKey) {
                Screens.DRAWER_FLOW -> DrawerFlowFragment()
                Screens.MAIN_FLOW -> MainFlowFragment()
                Screens.ABOUT_SCREEN -> AboutFragment()
                Screens.AUTH_FLOW -> AuthFlowFragment()
                Screens.FEED_SCREEN -> MyFeedFragment()
                Screens.SUBSCRIPTIONS_SCREEN -> MyFeedFragment()
                Screens.TRENDING_SCREEN -> MyFeedFragment()
                Screens.LIBRARY_SCREEN -> LibraryContainerFragment()
                Screens.AUTH_SCREEN -> AuthFragment()
                Screens.USER_FLOW -> UserFlowFragment.create(data as Long, DI.USER_FLOW_SCOPE + " " + System.currentTimeMillis())
                Screens.USER_INFO_SCREEN -> UserInfoFragment.create(data as String)
                Screens.FOLLOWERS_SCREEN -> {
                    val holder: FollowersHolder = data as FollowersHolder
                    FollowersContainerFragment.newInstance(holder.state, holder.userId, holder.scope)
                }
                Screens.LIKES_SCREEN -> (data as Pair<Long, String>).let { (userId, scope) -> LikesFragment.createNewInstance(userId, scope) }
                else -> null
            }
    fun createIntent(flowKey: String, data: Any?) = null
}