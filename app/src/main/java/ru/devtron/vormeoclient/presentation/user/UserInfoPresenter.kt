package ru.devtron.vormeoclient.presentation.user

import com.arellomobile.mvp.InjectViewState
import io.reactivex.rxkotlin.plusAssign
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.UserId
import ru.devtron.vormeoclient.entity.user.FollowersHolder
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.model.interactor.user.UserInteractor
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import ru.devtron.vormeoclient.presentation.global.ErrorHandler
import ru.devtron.vormeoclient.presentation.global.GlobalVideoController
import ru.devtron.vormeoclient.presentation.global.Paginator
import javax.inject.Inject

/**
 * Created by ruslanaliev on 11.12.2017.
 */
@InjectViewState
class UserInfoPresenter @Inject constructor(
        private val userInteractor: UserInteractor,
        private val router: FlowRouter,
        private val errorHandler: ErrorHandler,
        @UserId userIdWrapper: PrimitiveWrapper<Long>,
        private val globalVideoController: GlobalVideoController
) : BasePresenter<UserInfoView>() {
    private val userId = userIdWrapper.value

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        disposables += userInteractor
                .getUser(userId)
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                        { viewState.showUser(it) },
                        { errorHandler.proceed(it) { viewState.showMessage(it) } }
                )
        paginator.refresh()
    }

    fun showFollowers(args: Int, scopeName: String?) = router.navigateTo(Screens.FOLLOWERS_SCREEN, FollowersHolder(userId, args, scopeName))

    fun onBackPressed() = router.finishFlow()

    fun onFollowClick(checked: Boolean) {
        disposables += userInteractor.followUser(userId, checked)
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                        { viewState.isFollowing(!checked) },
                        { errorHandler.proceed(it) { viewState.showMessage(it) } }
                )
    }

    fun showLikes(scopeName: String?) = router.navigateTo(Screens.LIKES_SCREEN, Pair(userId, scopeName))

    private val paginator = Paginator(
            { userInteractor.getUserVideos(userIdWrapper.value, it) },
            object : Paginator.ViewController<Video> {
                override fun showEmptyProgress(show: Boolean) {
                    viewState.showEmptyProgress(show)
                }

                override fun showEmptyError(show: Boolean, error: Throwable?) {
                    if (error != null) {
                        errorHandler.proceed(error) { viewState.showEmptyError(show, it) }
                    } else {
                        viewState.showEmptyError(show, null)
                    }
                }

                override fun showErrorMessage(error: Throwable) {
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }

                override fun showEmptyView(show: Boolean) {
                    viewState.showEmptyView(show)
                }

                override fun showData(show: Boolean, data: List<Video>) {
                    viewState.showVideos(show, data)
                }

                override fun showRefreshProgress(show: Boolean) {
                    viewState.showRefreshProgress(show)
                }

                override fun showPageProgress(show: Boolean) {
                    viewState.showPageProgress(show)
                }
            }
    )

    fun onVideoClick(video: Video) { globalVideoController.openVideo(video ) }
    fun profileClick(userId: Long) = router.startFlow(Screens.USER_FLOW, userId)
    fun loadNextLikesPage() = paginator.loadNewPage()

    override fun onDestroy() {
        super.onDestroy()
        paginator.release()
    }
}