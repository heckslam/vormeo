package ru.devtron.vormeoclient.model.repository.feed

import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.DefaultPageSize
import ru.devtron.vormeoclient.entity.Feed
import ru.devtron.vormeoclient.model.data.server.VimeoApi
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import java.text.SimpleDateFormat
import javax.inject.Inject

class FeedRepository @Inject constructor(
        private val api: VimeoApi,
        private val schedulers: SchedulersProvider,
        @DefaultPageSize private val defaultPageSizeWrapper: PrimitiveWrapper<Int>
) {
    private val defaultPageSize = defaultPageSizeWrapper.value
    private val dayFormat = SimpleDateFormat("yyyy-MM-dd")

    fun getFeeds(
            page: Int,
            pageSize: Int = defaultPageSize
    ) = api
            .getFeed(
                    page,
                    pageSize
            )
            .map { t: Feed -> t.feedElements }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
}