package ru.devtron.vormeoclient.extension

import io.reactivex.Flowable
import io.reactivex.Observable
import ru.devtron.vormeoclient.model.system.None
import ru.devtron.vormeoclient.model.system.Optional
import ru.devtron.vormeoclient.model.system.Some

/**
 * Created on 11.09.2018 by ruslanaliev.
 */
private inline fun <reified R : Any> Observable<*>.ofType(): Observable<R> = ofType(R::class.java)
private inline fun <reified R : Any> Flowable<*>.ofType(): Flowable<R> = ofType(R::class.java)

/**
 * Filters items emitted by an ObservableSource by only emitting those that are `Some`.

 * @return an Observable that emits `Some.value` of those items emitted by the source ObservableSource that are `Some`.
 */
fun <T : Any> Observable<out Optional<T>>.filterSome(): Observable<T> = ofType<Some<T>>().map { it.value }

/**
 * Filters items emitted by a Publisher by only emitting those that are `Some`.
 * @return an Flowable that emits `Some.value` of those items emitted by the source Publisher that are `Some`.
 */
fun <T : Any> Flowable<out Optional<T>>.filterSome(): Flowable<T> = ofType<Some<T>>().map { it.value }

/**
 * Filters items emitted by an ObservableSource by only emitting those that are `None`.
 * @return an Observable that emits `Unit` for each item emitted by the source ObservableSource that is `None`.
 */
fun <T : Any> Observable<out Optional<T>>.filterNone(): Observable<Unit> = ofType<None>().map { Unit }

/**
 * Filters items emitted by an Publisher by only emitting those that are `None`.
 * @return an Flowable that emits `Unit` for each item emitted by the source Publisher that is `None`.
 */
fun <T : Any> Flowable<out Optional<T>>.filterNone(): Flowable<Unit> = ofType<None>().map { Unit }