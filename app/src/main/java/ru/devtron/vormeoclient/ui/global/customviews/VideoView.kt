package ru.devtron.vormeoclient.ui.global.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import com.google.android.exoplayer2.ui.PlayerView

/**
 * Created on 10.09.2018 by ruslanaliev.
 */
class VideoView(context: Context, attributeSet: AttributeSet? = null) : PlayerView(context, attributeSet) {

    fun setEndPadding(value: Float) {
        setPadding(0, 0, value.toInt(), 0)
    }

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        return false
    }
}