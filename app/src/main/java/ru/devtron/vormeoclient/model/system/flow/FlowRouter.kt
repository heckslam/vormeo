package ru.devtron.vormeoclient.model.system.flow

class FlowRouter(private val appRouter: AppRouter) : AppRouter() {

    fun startFlow(screenKey: String, data: Any? = null) {
        appRouter.navigateTo(screenKey, data)
    }

    fun newRootFlow(screenKey: String, data: Any? = null) {
        appRouter.newRootScreen(screenKey, data)
    }

    fun finishFlow() {
        appRouter.exit()
    }
}