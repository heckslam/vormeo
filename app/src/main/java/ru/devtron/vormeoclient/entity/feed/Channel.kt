package ru.devtron.vormeoclient.entity.feed

import com.google.gson.annotations.SerializedName
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.entity.picture.PictureMe

data class Channel (
        @SerializedName("uri") var uri: String,
        @SerializedName("name") var name: String,
        @SerializedName("description") var description: String,
        @SerializedName("link") var link: String,
        @SerializedName("created_time") var createdTime: String,
        @SerializedName("modified_time") var modifiedTime: String,
        @SerializedName("user") var user: AuthorizedUserMe,
        @SerializedName("pictures") var pictures: PictureMe,
        @SerializedName("header") var header: Any,
        @SerializedName("metadata") var metadata: MetaInfo,
        @SerializedName("resource_key") var resourceKey: String
)