package ru.devtron.vormeoclient.presentation.drawer

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devtron.vormeoclient.model.interactor.profile.MyUserInfo

@StateStrategyType(AddToEndSingleStrategy::class)
interface NavigationDrawerView : MvpView {
    enum class MenuItem {
        WATCH_NOW,
        PROFILE,
        FEEDBACK,
        ABOUT
    }

    fun showUserInfo(user: MyUserInfo)
    fun selectMenuItem(item: MenuItem)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showRatingDialog()
}