package ru.devtron.vormeoclient.model.repository.auth

import ru.devtron.vormeoclient.di.qualifier.DefaultServerPath
import ru.devtron.vormeoclient.model.data.auth.AuthHolder
import ru.devtron.vormeoclient.model.data.server.VimeoApi
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import javax.inject.Inject


class AuthRepository @Inject constructor(
        private val authData: AuthHolder,
        private val api: VimeoApi,
        private val schedulers: SchedulersProvider,
        @DefaultServerPath private val defaultServerPath: String
) {

    val isSignedIn get() = !authData.token.isNullOrEmpty()
    val userId get() = authData.userId

    fun requestOAuthToken(
            code: String,
            redirectUri: String
    ) = api
            .auth(code, redirectUri)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun saveAuthData(
            token: String,
            userId: Long,
            serverPath: String
    ) {
        authData.token = token
        authData.serverPath = serverPath
        authData.userId = userId
    }

    fun clearAuthData() {
        authData.token = null
        authData.serverPath = defaultServerPath
    }
}