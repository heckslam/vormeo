package ru.devtron.vormeoclient.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ProjectListMode