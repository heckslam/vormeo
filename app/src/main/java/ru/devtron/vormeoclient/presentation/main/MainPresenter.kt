package ru.devtron.vormeoclient.presentation.main

import com.arellomobile.mvp.MvpPresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class MainPresenter @Inject constructor(
        private val router: Router
) : MvpPresenter<MainView>() {

    fun onBackPressed() = router.exit()
}