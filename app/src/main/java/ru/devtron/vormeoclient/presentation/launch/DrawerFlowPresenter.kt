package ru.devtron.vormeoclient.presentation.launch

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.devtron.vormeoclient.di.DI
import ru.terrakok.cicerone.Router
import timber.log.Timber
import toothpick.Toothpick
import javax.inject.Inject

@InjectViewState
class DrawerFlowPresenter @Inject constructor(
        private val router: Router
) : MvpPresenter<MvpView>() {

    override fun onDestroy() {
        Toothpick.closeScope(DI.DRAWER_FLOW_SCOPE)
        Timber.d("DrawerFlow is dead")
        super.onDestroy()
    }

    fun onExit() {
        Timber.d("DrawerFlow is exit")
        router.exit()
    }
}