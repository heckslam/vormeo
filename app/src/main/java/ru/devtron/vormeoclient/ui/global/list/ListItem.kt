package ru.devtron.vormeoclient.ui.global.list

import ru.devtron.vormeoclient.entity.feed.FeedElement
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.entity.video.Video

sealed class ListItem {
    class ProgressItem : ListItem()
    class FeedItem(val feed: FeedElement) : ListItem()
    class VideoItem(val video: Video) : ListItem()
    class FollowerItem(val follower: AuthorizedUserMe) : ListItem()
}