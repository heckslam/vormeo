package ru.devtron.vormeoclient.presentation.app

import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.model.interactor.auth.AuthInteractor
import ru.devtron.vormeoclient.model.system.flow.AppRouter
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import javax.inject.Inject

class AppPresenter @Inject constructor(
        private val router: AppRouter,
        private val authInteractor: AuthInteractor
) : BasePresenter<AppView>() {

    fun coldStart() {
        val rootScreen =
                if (authInteractor.isSignedIn()) Screens.DRAWER_FLOW
                else Screens.AUTH_FLOW

        router.newRootScreen(rootScreen)
    }
}