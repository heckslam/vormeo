package ru.devtron.vormeoclient.presentation.auth

import com.arellomobile.mvp.InjectViewState
import io.reactivex.rxkotlin.plusAssign
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.model.interactor.auth.AuthInteractor
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import ru.devtron.vormeoclient.presentation.global.ErrorHandler
import javax.inject.Inject

@InjectViewState
class AuthPresenter @Inject constructor(
        private val router: FlowRouter,
        private val authInteractor: AuthInteractor,
        private val errorHandler: ErrorHandler
) : BasePresenter<AuthView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        startAuthorization()
    }

    private fun startAuthorization() {
        viewState.loadUrl(authInteractor.oauthUrl)
    }

    private fun requestToken(url: String) {
        disposables += authInteractor.login(url)
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                        { router.newRootFlow(Screens.DRAWER_FLOW) },
                        { errorHandler.proceed(it) { viewState.showMessage(it) } }
                )
    }

    fun onRedirect(url: String): Boolean {
        return if (authInteractor.checkOAuthRedirect(url)) {
            requestToken(url)
            true
        } else {
            viewState.loadUrl(url)
            false
        }
    }

    fun onBackPressed() = router.exit()
}