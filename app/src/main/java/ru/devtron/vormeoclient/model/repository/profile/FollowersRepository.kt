package ru.devtron.vormeoclient.model.repository.profile

import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.DefaultPageSize
import ru.devtron.vormeoclient.di.qualifier.UserId
import ru.devtron.vormeoclient.entity.Feed
import ru.devtron.vormeoclient.entity.user.Followers
import ru.devtron.vormeoclient.model.data.server.VimeoApi
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import java.util.*
import javax.inject.Inject

/**
 * Created by ruslanaliev on 12.12.2017.
 */
class FollowersRepository @Inject constructor(
        private val api: VimeoApi,
        private val schedulers: SchedulersProvider,
        @DefaultPageSize private val defaultPageSizeWrapper: PrimitiveWrapper<Int>
) {
    private val defaultPageSize = defaultPageSizeWrapper.value

    fun getFollowerRequests(
            userId: Long,
            page: Int,
            pageSize: Int = defaultPageSize
    ) = api
            .getFollowers(
                    userId,
                    page,
                    pageSize
            )
            .map { t: Followers -> t.followers }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun getFollowingRequests(
            userId: Long,
            page: Int,
            pageSize: Int = defaultPageSize
    ) = api
            .getFollowing(
                    userId,
                    page,
                    pageSize
            )
            .map { t: Followers -> t.followers }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

}