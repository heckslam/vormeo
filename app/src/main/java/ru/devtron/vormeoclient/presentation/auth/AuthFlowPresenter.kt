package ru.devtron.vormeoclient.presentation.auth

import com.arellomobile.mvp.MvpView
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import ru.terrakok.cicerone.Router
import toothpick.Toothpick
import javax.inject.Inject

/**
 * Created on 06.09.2018 by ruslanaliev.
 */
class AuthFlowPresenter @Inject constructor(
        private val router: Router
) : BasePresenter<MvpView>() {

    override fun onDestroy() {
        Toothpick.closeScope(DI.AUTH_FLOW_SCOPE)
        super.onDestroy()
    }

    fun onExit() {
        router.exit()
    }
}