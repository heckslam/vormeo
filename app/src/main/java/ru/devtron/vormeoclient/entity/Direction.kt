package ru.devtron.vormeoclient.entity

import com.google.gson.annotations.SerializedName

/**
 * Created by ruslanaliev on 13.12.2017.
 */
enum class Direction(private val jsonName: String) {
    @SerializedName("asc") ASC("asc"),
    @SerializedName("desc") DESC("desc");

    override fun toString() = jsonName
}