package ru.devtron.vormeoclient.model.interactor.profile

import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe

data class MyUserInfo(val user: AuthorizedUserMe?, val serverName: String)