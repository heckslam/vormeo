package ru.devtron.vormeoclient.entity.user

import com.google.gson.annotations.SerializedName
import ru.devtron.vormeoclient.entity.picture.Picture
import java.util.*

data class AuthorizedUser (
        @SerializedName("uri") val uri: String,
        @SerializedName("name") val name: String,
        @SerializedName("link") val link: String,
        @SerializedName("location") val location: String,
        @SerializedName("bio") val bio: String,
        @SerializedName("created_time") val createdTime: Date,
        @SerializedName("account") val account: String,
        @SerializedName("pictures") val pictures: List<Picture>?
) {
    val userId: Long
        get() = uri.substringAfterLast("/").toLong()
}