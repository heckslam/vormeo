package ru.devtron.vormeoclient.model.data.server

class ServerError(val errorCode: Int) : RuntimeException()