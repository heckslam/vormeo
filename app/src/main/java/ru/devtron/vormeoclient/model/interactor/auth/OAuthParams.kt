package ru.devtron.vormeoclient.model.interactor.auth

data class OAuthParams(
        val appId: String,
        val appKey: String,
        val redirectUrl: String,
        val scopes: String
)