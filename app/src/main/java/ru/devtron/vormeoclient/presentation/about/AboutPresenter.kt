package ru.devtron.vormeoclient.presentation.about

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.devtron.vormeoclient.BuildConfig
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.global.GlobalMenuController
import javax.inject.Inject

@InjectViewState
class AboutPresenter @Inject constructor(
        private val router: FlowRouter,
        private val menuController: GlobalMenuController
) : MvpPresenter<AboutView>() {

    override fun onFirstViewAttach() {
        viewState.showAppVersion("${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})")
    }

    fun onMenuPressed() = menuController.open()
    fun onBackPressed() = router.exit()
}