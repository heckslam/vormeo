package ru.devtron.vormeoclient.entity

import com.google.gson.annotations.SerializedName
import ru.devtron.vormeoclient.entity.feed.FeedElement

data class Feed (
    @SerializedName("total") val total: Int,
    @SerializedName("page") val page: Int,
    @SerializedName("per_page") val perPage: Int,
    @SerializedName("paging") val paging: Paging,
    @SerializedName("data") val feedElements: List<FeedElement>
)