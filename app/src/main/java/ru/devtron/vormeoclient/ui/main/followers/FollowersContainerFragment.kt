package ru.devtron.vormeoclient.ui.main.followers

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_follow_container.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.extension.argument
import ru.devtron.vormeoclient.presentation.main.followers.FollowersContainerPresenter
import ru.devtron.vormeoclient.presentation.main.followers.FollowersContainerView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import toothpick.Toothpick

/**
 * Created by ruslanaliev on 12.12.2017.
 */
class FollowersContainerFragment : BaseFragment(), FollowersContainerView {
    override val layoutRes = R.layout.fragment_follow_container

    private val adapter: FollowersAdapter by lazy { FollowersAdapter() }

    @InjectPresenter lateinit var presenter: FollowersContainerPresenter

    private val scopeName: String? by argument(ARG_SCOPE_NAME)

    @ProvidePresenter
    fun providePresenter(): FollowersContainerPresenter {
        return Toothpick
                .openScope(scopeName)
                .getInstance(FollowersContainerPresenter::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScope(DI.DRAWER_FLOW_SCOPE))
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
        viewPager.adapter = adapter
        viewPager.currentItem = arguments?.getInt(ARG_FOLLOWER_STATE) ?: 0
    }

    private inner class FollowersAdapter : FragmentPagerAdapter(childFragmentManager) {
        val userId: Long = arguments?.getLong(ARG_USER_ID)!!
        private val pages = listOf<Fragment>(
                    FollowersFragment.newInstance(FollowersFragment.ARG_FOLLOWERS, userId),
                    FollowersFragment.newInstance(FollowersFragment.ARG_FOLLOWING, userId)
        )

        private val pageTitles = listOf(
                getString(R.string.screen_followers),
                getString(R.string.screen_following)
        )

        override fun getItem(position: Int) = pages[position]

        override fun getCount() = pages.size

        override fun getPageTitle(position: Int) = pageTitles[position]
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    companion object {
        private const val ARG_USER_ID = "arg_user_id"
        private const val ARG_FOLLOWER_STATE = "ARG_FOLLOWER_STATE"
        private const val ARG_SCOPE_NAME = "arg_scope_name"

        fun newInstance(state: Int, userId: Long, scope: String?) = FollowersContainerFragment().apply {
            arguments = Bundle().apply {
                putLong(ARG_USER_ID, userId)
                putInt(ARG_FOLLOWER_STATE, state)
                putString(ARG_SCOPE_NAME, scope)
            }
        }
    }
}