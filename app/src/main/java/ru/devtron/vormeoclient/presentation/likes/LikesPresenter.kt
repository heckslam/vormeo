package ru.devtron.vormeoclient.presentation.likes

import com.arellomobile.mvp.InjectViewState
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.UserId
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.model.interactor.like.LikeInteractor
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import ru.devtron.vormeoclient.presentation.global.ErrorHandler
import ru.devtron.vormeoclient.presentation.global.GlobalVideoController
import ru.devtron.vormeoclient.presentation.global.Paginator
import javax.inject.Inject

@InjectViewState
class LikesPresenter @Inject constructor(
        private val router: FlowRouter,
        private val likeInteractor: LikeInteractor,
        private val errorHandler: ErrorHandler,
        @UserId userIdWrapper: PrimitiveWrapper<Long>,
        private val globalVideoController: GlobalVideoController
) : BasePresenter<LikesView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refreshLikes()
    }

    private val paginator = Paginator(
            { likeInteractor.getLikes(userIdWrapper.value, it) },
            object : Paginator.ViewController<Video> {
                override fun showEmptyProgress(show: Boolean) {
                    viewState.showEmptyProgress(show)
                }

                override fun showEmptyError(show: Boolean, error: Throwable?) {
                    if (error != null) {
                        errorHandler.proceed(error) { viewState.showEmptyError(show, it) }
                    } else {
                        viewState.showEmptyError(show, null)
                    }
                }

                override fun showErrorMessage(error: Throwable) {
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }

                override fun showEmptyView(show: Boolean) {
                    viewState.showEmptyView(show)
                }

                override fun showData(show: Boolean, data: List<Video>) {
                    viewState.showVideos(show, data)
                }

                override fun showRefreshProgress(show: Boolean) {
                    viewState.showRefreshProgress(show)
                }

                override fun showPageProgress(show: Boolean) {
                    viewState.showPageProgress(show)
                }
            }
    )

    fun onVideoClick(video: Video) { globalVideoController.openVideo(video) }
    fun profileClick(userId: Long) = router.startFlow(Screens.USER_FLOW, userId)
    fun refreshLikes() = paginator.refresh()
    fun loadNextLikesPage() = paginator.loadNewPage()

    override fun onDestroy() {
        super.onDestroy()
        paginator.release()
    }

    fun onBackPressed() = router.backTo(Screens.USER_INFO_SCREEN)

}