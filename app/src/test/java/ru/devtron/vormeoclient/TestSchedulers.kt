package ru.devtron.vormeoclient

import io.reactivex.schedulers.Schedulers
import ru.devtron.vormeoclient.model.system.SchedulersProvider

class TestSchedulers : SchedulersProvider {
    override fun ui() = Schedulers.trampoline()
    override fun computation() = Schedulers.trampoline()
    override fun trampoline() = Schedulers.trampoline()
    override fun newThread() = Schedulers.trampoline()
    override fun io() = Schedulers.trampoline()
}