package ru.devtron.vormeoclient.entity.user

import com.google.gson.annotations.SerializedName
import ru.devtron.vormeoclient.entity.Paging

/**
 * Created by ruslanaliev on 13.12.2017.
 */
data class Followers (
        @SerializedName("total") val total: Int,
        @SerializedName("page") val page: Int,
        @SerializedName("per_page") val perPage: Int,
        @SerializedName("paging") val paging: Paging,
        @SerializedName("data") val followers: List<AuthorizedUserMe>
)