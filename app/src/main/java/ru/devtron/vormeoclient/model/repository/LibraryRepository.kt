package ru.devtron.vormeoclient.model.repository

import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.DefaultPageSize
import ru.devtron.vormeoclient.entity.Direction
import ru.devtron.vormeoclient.model.data.auth.AuthHolder
import ru.devtron.vormeoclient.model.data.server.VimeoApi
import ru.devtron.vormeoclient.model.data.storage.Prefs
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import javax.inject.Inject

/**
 * Created by ruslanaliev on 13.12.2017.
 */
class LibraryRepository @Inject constructor(
        private val api: VimeoApi,
        private val schedulers: SchedulersProvider,
        @DefaultPageSize private val defaultPageSizeWrapper: PrimitiveWrapper<Int>,
        private val authData: AuthHolder
) {

    private val defaultPageSize = defaultPageSizeWrapper.value

    fun getWatchLater(
            direction: Direction? = null,
            page: Int,
            pageSize: Int = defaultPageSize
    ) = api
            .getWatchLater(
                    direction,
                    page,
                    pageSize
            )
            .map { items -> items.videoList }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun getLikes(
            direction: Direction? = null,
            page: Int,
            pageSize: Int = defaultPageSize
    ) = api
            .getLikes(
                    authData.userId,
                    direction,
                    page,
                    pageSize
            )
            .map { items -> items.videoList }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
}
