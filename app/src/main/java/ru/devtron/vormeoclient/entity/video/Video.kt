package ru.devtron.vormeoclient.entity.video

import android.nfc.Tag
import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalDateTime
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.entity.picture.PictureMe
import ru.devtron.vormeoclient.entity.feed.MetaInfo

data class Video (
        @SerializedName("uri") val uri: String,
        @SerializedName("name") val name: String,
        @SerializedName("description") val description: String,
        @SerializedName("link") val link: String,
        @SerializedName("duration") val duration: Long,
        @SerializedName("width") val width: Int,
        @SerializedName("language") val language: String,
        @SerializedName("height") val height: Int,
        @SerializedName("embed") val embed: Embed,
        @SerializedName("created_time") val createdTime: LocalDateTime,
        @SerializedName("modified_time") val modifiedTime: String,
        @SerializedName("content_rating") val contentRating: List<String>,
        @SerializedName("pictures") val pictures: PictureMe,
        @SerializedName("tags") val tags: List<Tag>,
        @SerializedName("stats") val stats: Stats,
        @SerializedName("metadata") val metadata: MetaInfo,
        @SerializedName("user") val user: AuthorizedUserMe,
        @SerializedName("status") val status: String
)