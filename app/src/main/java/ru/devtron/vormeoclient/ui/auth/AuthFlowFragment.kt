package ru.devtron.vormeoclient.ui.auth

import android.os.Bundle
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.di.module.FlowNavigationModule
import ru.devtron.vormeoclient.extension.setLaunchScreen
import ru.devtron.vormeoclient.model.system.flow.AppRouter
import ru.devtron.vormeoclient.presentation.auth.AuthFlowPresenter
import ru.devtron.vormeoclient.ui.global.FlowFragment
import toothpick.Toothpick

/**
 * Created on 06.09.2018 by ruslanaliev.
 */
class AuthFlowFragment : FlowFragment(), MvpView {

    @InjectPresenter
    lateinit var presenter: AuthFlowPresenter

    @ProvidePresenter
    fun providePresenter() =
            Toothpick.openScope(DI.AUTH_FLOW_SCOPE)
                    .getInstance(AuthFlowPresenter::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        initScope()
        super.onCreate(savedInstanceState)
        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.AUTH_SCREEN, null)
        }
    }

    private fun initScope() {
        val scope = Toothpick.openScopes(DI.SERVER_SCOPE, DI.AUTH_FLOW_SCOPE)
        scope.installModules(
                FlowNavigationModule(scope.getInstance(AppRouter::class.java))
        )
        Toothpick.inject(this, scope)
    }

    override fun onExit() {
        presenter.onExit()
    }
}