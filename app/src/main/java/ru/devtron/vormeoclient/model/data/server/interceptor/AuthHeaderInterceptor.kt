package ru.devtron.vormeoclient.model.data.server.interceptor

import android.util.Base64
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response
import ru.devtron.vormeoclient.BuildConfig
import ru.devtron.vormeoclient.model.data.auth.AuthHolder

class AuthHeaderInterceptor(private val authData: AuthHolder) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = if (authData.token != null) {
            request.newBuilder().addHeader("Authorization", "Bearer " + authData.token).addHeader("Accept", "application/json").build()
        } else{
            val credentials = BuildConfig.VIMEO_CLIENT_ID + ":" + BuildConfig.VIMEO_CLIENT_SECRET
            val authorization = "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            request.newBuilder().addHeader("Authorization", authorization).addHeader("Accept", "application/json").build()
        }
        return chain.proceed(request)
    }
}