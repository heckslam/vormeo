package ru.devtron.vormeoclient.model.interactor.profile.followers

import io.reactivex.Single
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.model.repository.profile.FollowersRepository
import ru.devtron.vormeoclient.ui.main.followers.FollowersFragment
import javax.inject.Inject

/**
 * Created by ruslanaliev on 12.12.2017.
 */
class FollowersListInteractor @Inject constructor(
        private val followersRepository: FollowersRepository
) {
    fun getMyFollowerRequests(
            state: Int,
            userId: Long,
            page: Int
    ): Single<List<AuthorizedUserMe>> {
        return if (state == FollowersFragment.ARG_FOLLOWERS) {
            followersRepository
                    .getFollowerRequests(
                            page = page,
                            userId = userId
                    )
        } else {
            followersRepository
                    .getFollowingRequests(
                            page = page,
                            userId = userId
                    )
        }
    }
}