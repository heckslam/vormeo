package ru.devtron.vormeoclient.model.repository.like

import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.DefaultPageSize
import ru.devtron.vormeoclient.entity.Direction
import ru.devtron.vormeoclient.entity.VideoWrapper
import ru.devtron.vormeoclient.model.data.server.VimeoApi
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import javax.inject.Inject

/**
 * Created by ruslanaliev on 13.12.2017.
 */
class LikeRepository @Inject constructor(
        private val api: VimeoApi,
        private val schedulers: SchedulersProvider,
        @DefaultPageSize private val defaultPageSizeWrapper: PrimitiveWrapper<Int>
) {
    private val defaultPageSize = defaultPageSizeWrapper.value

    fun getLikes(
            userId: Long,
            direction: Direction? = null,
            page: Int,
            pageSize: Int = defaultPageSize
    ) = api
            .getLikes(
                    userId,
                    direction,
                    page,
                    pageSize
            )
            .map { t: VideoWrapper -> t.videoList }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
}