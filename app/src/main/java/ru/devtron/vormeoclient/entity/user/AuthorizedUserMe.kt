package ru.devtron.vormeoclient.entity.user

import com.google.gson.annotations.SerializedName
import ru.devtron.vormeoclient.entity.feed.MetaInfo
import ru.devtron.vormeoclient.entity.picture.PictureMe
import java.util.*

data class AuthorizedUserMe(
        @SerializedName("uri") val uri: String,
        @SerializedName("name") val name: String,
        @SerializedName("link") val link: String,
        @SerializedName("location") val location: String,
        @SerializedName("bio") val bio: String,
        @SerializedName("created_time") val createdTime: Date,
        @SerializedName("account") val accountType: AccountType?,
        @SerializedName("pictures") val pictures: PictureMe?,
        @SerializedName("metadata") var metadata: UserMetaData
        ) {
    val userId: Long
        get() = uri.substringAfterLast("/").toLong()
    val avatarUrl: String
        get() = pictures?.sizes?.last()?.link ?: ""
}