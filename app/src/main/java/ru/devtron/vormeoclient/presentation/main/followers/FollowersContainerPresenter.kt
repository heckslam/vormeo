package ru.devtron.vormeoclient.presentation.main.followers

import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import javax.inject.Inject

/**
 * Created by ruslanaliev on 13.12.2017.
 */
class FollowersContainerPresenter @Inject constructor(
        private val router: FlowRouter
) : BasePresenter<FollowersContainerView>() {

    fun onBackPressed() = router.backTo(Screens.USER_INFO_SCREEN)
}