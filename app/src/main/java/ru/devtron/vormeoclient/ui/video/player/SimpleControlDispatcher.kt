package ru.devtron.vormeoclient.ui.video.player

import com.google.android.exoplayer2.DefaultControlDispatcher
import com.google.android.exoplayer2.Player

/**
 * Created on 10.09.2018 by ruslanaliev.
 */
class SimpleControlDispatcher(
        private val externalLinkListener: (String) -> Unit
) : DefaultControlDispatcher() {

    var isExternal: Boolean = false
    var url: String = ""

    override fun dispatchSetPlayWhenReady(
            player: Player?,
            playWhenReady: Boolean
    ) = super.dispatchSetPlayWhenReady(
            player,
            if (isExternal) {
                externalLinkListener(url)
                false
            } else playWhenReady
    )
}