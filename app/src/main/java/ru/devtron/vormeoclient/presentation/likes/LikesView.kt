package ru.devtron.vormeoclient.presentation.likes

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devtron.vormeoclient.entity.video.Video

@StateStrategyType(AddToEndSingleStrategy::class)
interface LikesView : MvpView {
    fun showRefreshProgress(show: Boolean)
    fun showEmptyProgress(show: Boolean)
    fun showPageProgress(show: Boolean)
    fun showEmptyView(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showVideos(show: Boolean, videos: List<Video>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}