package ru.devtron.vormeoclient.entity.user

import com.google.gson.annotations.SerializedName
import ru.devtron.vormeoclient.entity.feed.Interaction

/**
 * Created by ruslanaliev on 13.12.2017.
 */
data class UserInteractions(
        @SerializedName("follow") val follow: Interaction
)