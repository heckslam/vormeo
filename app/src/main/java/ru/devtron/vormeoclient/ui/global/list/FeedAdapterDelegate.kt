package ru.devtron.vormeoclient.ui.global.list

import android.content.res.Resources
import android.graphics.Bitmap
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import kotlinx.android.synthetic.main.item_feed.view.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.entity.feed.FeedElement
import ru.devtron.vormeoclient.extension.formatDuration
import ru.devtron.vormeoclient.extension.humanTime
import ru.devtron.vormeoclient.extension.humanViewCount
import ru.devtron.vormeoclient.extension.inflate
import ru.devtron.vormeoclient.ui.global.glide.GlideApp

class FeedAdapterDelegate(private val clickListener: (FeedElement) -> Unit,
                          private val profileClickListener: (Long) -> Unit) : AdapterDelegate<MutableList<ListItem>>() {

    override fun isForViewType(items: MutableList<ListItem>, position: Int) =
            items[position] is ListItem.FeedItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
            FeedViewHolder(parent.inflate(R.layout.item_feed), clickListener, profileClickListener)

    override fun onBindViewHolder(items: MutableList<ListItem>,
                                  position: Int,
                                  viewHolder: RecyclerView.ViewHolder,
                                  payloads: MutableList<Any>) =
            (viewHolder as FeedViewHolder).bind((items[position] as ListItem.FeedItem).feed)

    private class FeedViewHolder(val view: View, clickListener: (FeedElement) -> Unit,
                                 profileClickListener: (Long) -> Unit) : RecyclerView.ViewHolder(view) {
        private lateinit var feed: FeedElement

        init {
            view.setOnClickListener { clickListener.invoke(feed) }
            view.avatarIV.setOnClickListener { profileClickListener.invoke(feed.clip.user.userId) }
        }

        fun bind(feed: FeedElement) {
            this.feed = feed

            val res = view.resources
            view.feedTitleTextView.text = feed.clip.name
            view.feedDescriptionTextView.text = createMessage(feed, res)
            view.durationTv.text = feed.clip.duration.formatDuration(feed.clip.duration)

            GlideApp.with(view.context)
                    .asBitmap()
                    .load(feed.clip.user.pictures?.sizes?.get(3)?.link)
                    .centerCrop()
                    .into(object : BitmapImageViewTarget(view.avatarIV) {
                        override fun setResource(resource: Bitmap?) {
                            resource?.let {
                                RoundedBitmapDrawableFactory.create(view.resources, it).run {
                                    this.isCircular = true
                                    view.avatarIV.setImageDrawable(this)
                                }
                            }
                        }
                    })

            GlideApp.with(view.context)
                    .load(feed.clip.pictures.sizes[3].link)
                    .centerCrop()
                    .into(view.videoPreviewImg)
        }

        private fun createMessage(feed: FeedElement, res: Resources): String {
            return res.getString(R.string.video_description_row,
                    feed.clip.user.name,
                    feed.clip.stats.plays.humanViewCount(res),
                    feed.time.humanTime(res))
        }
    }
}