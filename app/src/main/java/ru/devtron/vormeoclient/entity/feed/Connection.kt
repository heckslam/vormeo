package ru.devtron.vormeoclient.entity.feed

import com.google.gson.annotations.SerializedName

data class Connection (
    @SerializedName("uri") val uri: String,
    @SerializedName("options") val options: List<String>,
    @SerializedName("total") val total: Int
)