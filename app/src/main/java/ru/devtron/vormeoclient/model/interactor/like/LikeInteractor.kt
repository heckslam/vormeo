package ru.devtron.vormeoclient.model.interactor.like

import ru.devtron.vormeoclient.model.repository.like.LikeRepository
import javax.inject.Inject

/**
 * Created by ruslanaliev on 13.12.2017.
 */
class LikeInteractor @Inject constructor(
        private val likeRepository: LikeRepository
) {
    fun getLikes(userId: Long, page: Int) = likeRepository.getLikes(userId = userId, page = page)
}