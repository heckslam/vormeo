package ru.devtron.vormeoclient.model.data.server

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import ru.devtron.vormeoclient.entity.player.VideoConfig


interface VimeoPlayerApi {
    @GET("video/{videoId}/config")
    fun getVideoConfig(@Path("videoId") videoId: String): Single<VideoConfig>
}
const val BASE_URL = "http://player.vimeo.com/"