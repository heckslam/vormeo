package ru.devtron.vormeoclient.entity.feed

import com.google.gson.annotations.SerializedName

data class Connections (
    @SerializedName("comments") val comments: Connection,
    @SerializedName("pictures") val pictures: Connection,
    @SerializedName("texttracks") val texttracks: Connection
)