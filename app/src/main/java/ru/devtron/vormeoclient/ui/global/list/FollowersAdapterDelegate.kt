package ru.devtron.vormeoclient.ui.global.list

import android.graphics.Bitmap
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import kotlinx.android.synthetic.main.item_follower.view.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.extension.getIcon
import ru.devtron.vormeoclient.extension.inflate
import ru.devtron.vormeoclient.extension.showTextOrHide
import ru.devtron.vormeoclient.ui.global.glide.GlideApp

/**
 * Created by ruslanaliev on 12.12.2017.
 */
class FollowersAdapterDelegate(private val clickListener: (AuthorizedUserMe) -> Unit) : AdapterDelegate<MutableList<ListItem>>() {

    override fun isForViewType(items: MutableList<ListItem>, position: Int) =
            items[position] is ListItem.FollowerItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
            FollowersViewHolder(parent.inflate(R.layout.item_follower), clickListener)

    override fun onBindViewHolder(items: MutableList<ListItem>,
                                  position: Int,
                                  viewHolder: RecyclerView.ViewHolder,
                                  payloads: MutableList<Any>) =
            (viewHolder as FollowersViewHolder).bind((items[position] as ListItem.FollowerItem).follower)

    private class FollowersViewHolder(val view: View, clickListener: (AuthorizedUserMe) -> Unit) : RecyclerView.ViewHolder(view) {
        private lateinit var follower: AuthorizedUserMe

        init {
            view.setOnClickListener { clickListener(follower) }
        }

        fun bind(follower: AuthorizedUserMe) {
            this.follower = follower

            val res = view.resources
            view.usernameTextView.text = follower.name
            follower.accountType?.getIcon()?.let { view.userAccountTypeImageView.setImageResource(it) }
            view.userBioTv.showTextOrHide(follower.bio)
            view.userStatsTv.text = res.getString(R.string.follower_stats, follower.metadata.connections.videos.total,
                    follower.metadata.connections.followers.total)

            GlideApp.with(view.context)
                    .asBitmap()
                    .load(follower.pictures?.sizes?.get(3)?.link)
                    .centerCrop()
                    .into(object : BitmapImageViewTarget(view.userAvatarImageView) {
                        override fun setResource(resource: Bitmap?) {
                            resource?.let {
                                RoundedBitmapDrawableFactory.create(view.resources, it).run {
                                    this.isCircular = true
                                    view.userAvatarImageView.setImageDrawable(this)
                                }
                            }
                        }
                    })
        }
    }
}