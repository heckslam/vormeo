package ru.devtron.vormeoclient.di.provider

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.threeten.bp.LocalDateTime
import ru.devtron.vormeoclient.model.data.server.deserializer.DateDeserializer
import javax.inject.Inject
import javax.inject.Provider

class GsonProvider @Inject constructor() : Provider<Gson> {

    override fun get(): Gson =
        GsonBuilder()
            .registerTypeAdapter(LocalDateTime::class.java, DateDeserializer())
            .create()
}