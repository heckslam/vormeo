package ru.devtron.vormeoclient.ui.main.library

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.fragment_follow_container.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.global.GlobalMenuController
import ru.devtron.vormeoclient.presentation.main.followers.FollowersContainerView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import toothpick.Toothpick
import javax.inject.Inject

/**
 * Created by ruslanaliev on 12.12.2017.
 */
class LibraryContainerFragment : BaseFragment(), FollowersContainerView {
    @Inject
    lateinit var router: FlowRouter

    @Inject
    lateinit var menuController: GlobalMenuController

    override val layoutRes = R.layout.fragment_library_container

    private val adapter: LibraryAdapter by lazy { LibraryAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScope(DI.DRAWER_FLOW_SCOPE))
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.setNavigationOnClickListener { menuController.open() }
        viewPager.adapter = adapter
    }

    private inner class LibraryAdapter : FragmentPagerAdapter(childFragmentManager) {
        private val pages = listOf<Fragment>(
                LibraryListFragment.createNewInstance(LibraryListFragment.LIBRARY_STATE_WATCH_LATER),
                LibraryListFragment.createNewInstance(LibraryListFragment.LIBRARY_STATE_LIKES)
        )

        private val pageTitles = listOf(
                getString(R.string.library_watch_later),
                getString(R.string.library_likes)
        )

        override fun getItem(position: Int) = pages[position]

        override fun getCount() = pages.size

        override fun getPageTitle(position: Int) = pageTitles[position]
    }

    override fun onBackPressed() {
        router.exit()
    }
}