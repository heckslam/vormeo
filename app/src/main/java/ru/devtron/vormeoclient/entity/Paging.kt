package ru.devtron.vormeoclient.entity

import com.google.gson.annotations.SerializedName

data class Paging (
    @SerializedName("next") val next: String,
    @SerializedName("previous") val previous: String,
    @SerializedName("first") val first: String,
    @SerializedName("last") val last: String
)