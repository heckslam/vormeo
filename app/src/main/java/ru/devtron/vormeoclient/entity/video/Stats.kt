package ru.devtron.vormeoclient.entity.video

import com.google.gson.annotations.SerializedName

data class Stats (
    @SerializedName("plays") val plays: Int
)