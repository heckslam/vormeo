package ru.devtron.vormeoclient.model.repository.profile

import ru.devtron.vormeoclient.di.qualifier.ServerPath
import ru.devtron.vormeoclient.model.data.server.VimeoApi
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import javax.inject.Inject

class ProfileRepository @Inject constructor(
        @ServerPath private val serverPath: String,
        private val api: VimeoApi,
        private val schedulers: SchedulersProvider
) {

    fun getMyProfile() = api
            .getMyUser()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun getMyServerName() = serverPath
}