package ru.devtron.vormeoclient.di.module

import android.content.Context
import ru.devtron.vormeoclient.BuildConfig
import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.DefaultPageSize
import ru.devtron.vormeoclient.di.qualifier.DefaultServerPath
import ru.devtron.vormeoclient.model.data.auth.AuthHolder
import ru.devtron.vormeoclient.model.data.storage.Prefs
import ru.devtron.vormeoclient.model.system.AppSchedulers
import ru.devtron.vormeoclient.model.system.ResourceManager
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import ru.devtron.vormeoclient.model.system.flow.AppRouter
import ru.devtron.vormeoclient.model.system.message.SystemMessageNotifier
import ru.devtron.vormeoclient.presentation.global.GlobalVideoController
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module


class AppModule(context: Context) : Module() {
    init {
        //Global
        bind(Context::class.java).toInstance(context)
        bind(String::class.java).withName(DefaultServerPath::class.java).toInstance(BuildConfig.ORIGIN_VIMEO_ENDPOINT)
        bind(PrimitiveWrapper::class.java).withName(DefaultPageSize::class.java).toInstance(PrimitiveWrapper(20))
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())
        bind(ResourceManager::class.java).singletonInScope()
        bind(SystemMessageNotifier::class.java).toInstance(SystemMessageNotifier())
        bind(GlobalVideoController::class.java).toInstance(GlobalVideoController())

        //Navigation
        val cicerone = Cicerone.create(AppRouter())
        bind(Router::class.java).toInstance(cicerone.router)
        bind(AppRouter::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        //Auth
        bind(AuthHolder::class.java).to(Prefs::class.java).singletonInScope()
    }
}