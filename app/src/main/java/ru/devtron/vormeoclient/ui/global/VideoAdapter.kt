package ru.devtron.vormeoclient.ui.global

import android.support.v7.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.ui.global.list.LikeAdapterDelegate
import ru.devtron.vormeoclient.ui.global.list.ListItem
import ru.devtron.vormeoclient.ui.global.list.ProgressAdapterDelegate


class VideoAdapter(clickListener: (Video) -> Unit,
                   profileClickListener: (Long) -> Unit,
                   private val nextPageListener: () -> Unit) : ListDelegationAdapter<MutableList<ListItem>>() {
    init {
        items = mutableListOf()
        delegatesManager.addDelegate(LikeAdapterDelegate(clickListener, profileClickListener))
        delegatesManager.addDelegate(ProgressAdapterDelegate())
    }

    fun setData(feeds: List<Video>) {
        val progress = isProgress()

        items.clear()
        items.addAll(feeds.map { ListItem.VideoItem(it) })
        if (progress) items.add(ListItem.ProgressItem())

        notifyDataSetChanged()
    }

    fun showProgress(isVisible: Boolean) {
        val currentProgress = isProgress()

        if (isVisible && !currentProgress) items.add(ListItem.ProgressItem())
        else if (!isVisible && currentProgress) items.remove(items.last())

        notifyDataSetChanged()
    }

    private fun isProgress() = items.isNotEmpty() && items.last() is ListItem.ProgressItem

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any?>) {
        super.onBindViewHolder(holder, position, payloads)
        if (position == items.size - 10) nextPageListener
    }
}