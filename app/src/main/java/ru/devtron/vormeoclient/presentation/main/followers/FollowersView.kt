package ru.devtron.vormeoclient.presentation.main.followers

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe

/**
 * Created by ruslanaliev on 12.12.2017.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface FollowersView: MvpView {
    fun showRefreshProgress(show: Boolean)
    fun showEmptyProgress(show: Boolean)
    fun showPageProgress(show: Boolean)
    fun showEmptyView(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showFollowers(show: Boolean, followers: List<AuthorizedUserMe>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}