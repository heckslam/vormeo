# Vimeo client

## Libraries
* RxJava 2 - for reactive programming
* Toothpick - for dependency injection
* Retrofit 2 and Retrofit Mock mode for debug builds - for Networking
* Kotlin
* Gson - for parsing the JSON responses
* Moxy - for MVP pattern implementation
* Cicerone - for navigation
* Glide - for image loading
* JUnit - for unit testing
* MotionLayout - for youtube like animation (still **alpha**, may be bugs)
* ExoPlayer - for playing videos

## Roadmap

- [x] Feed screen
- [x] User authentication
- [x] Watch later screen
- [x] Likes screen
- [x] User profile screen
- [x] Followers screen
- [x] Video player screen
- [ ] Video details screen
- [ ] Commenting

# Setup

Create `vimeo_client_id`, `vimeo_client_secret`, `vimeo_callback_url` in **gradle.properties** file to yours.  For more information please visit [this page](http://developer.vimeo.com/).