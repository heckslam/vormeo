package ru.devtron.vormeoclient.ui.main

import android.os.Bundle
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.fragment_main.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.extension.color
import ru.devtron.vormeoclient.presentation.global.GlobalVideoController
import ru.devtron.vormeoclient.presentation.main.MainView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import toothpick.Toothpick
import javax.inject.Inject

class MainFlowFragment : BaseFragment(), MainView {
    override val layoutRes = R.layout.fragment_main

    private val currentTabFragment: BaseFragment?
        get() = childFragmentManager.fragments.firstOrNull { !it.isHidden } as? BaseFragment

    @Inject
    lateinit var globalVideoController: GlobalVideoController
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScope(DI.SERVER_SCOPE))
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AHBottomNavigationAdapter(activity, R.menu.navigation).apply {
            setupWithBottomNavigation(bottomBar)
        }
        with(bottomBar) {
            accentColor = context.color(R.color.colorPrimary)
            inactiveColor = context.color(R.color.secondary_text)

            setOnTabSelectedListener { position, wasSelected ->
                if (!wasSelected) selectTab(
                        when (position) {
                            0 -> Screens.FEED_SCREEN
                            1 -> Screens.TRENDING_SCREEN
                            2 -> Screens.SUBSCRIPTIONS_SCREEN
                            3 -> Screens.LIBRARY_SCREEN
                            else -> Screens.FEED_SCREEN
                        }
                )
                true
            }
        }

        selectTab(currentTabFragment?.tag ?: Screens.FEED_SCREEN)
    }

    override fun onResume() {
        super.onResume()
        compositeDisposable += globalVideoController.progressAnimState.subscribe {
            mainMotionLayout.progress = it
        }
    }

    override fun onPause() {
        super.onPause()
        compositeDisposable.dispose()
    }

    private fun selectTab(tab: String) {
        val currentFragment = currentTabFragment
        val newFragment = childFragmentManager.findFragmentByTag(tab)

        if (currentFragment != null && newFragment != null && currentFragment == newFragment) return

        childFragmentManager.beginTransaction().apply {
            if (newFragment == null) add(R.id.mainScreenContainer, createTabFragment(tab), tab)

            currentFragment?.let {
                hide(it)
                it.userVisibleHint = false
            }
            newFragment?.let {
                show(it)
                it.userVisibleHint = true
            }
        }.commitNow()
    }

    private fun createTabFragment(tab: String) =
            Screens.createFragment(tab) ?: throw RuntimeException("Unknown tab $tab")

    override fun onBackPressed() {
        currentTabFragment?.onBackPressed()
    }
}