package ru.devtron.vormeoclient.model.repository.user

import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.DefaultPageSize
import ru.devtron.vormeoclient.entity.Direction
import ru.devtron.vormeoclient.entity.VideoWrapper
import ru.devtron.vormeoclient.model.data.server.VimeoApi
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import javax.inject.Inject

class UserRepository @Inject constructor(
        private val api: VimeoApi,
        private val schedulers: SchedulersProvider,
        @DefaultPageSize private val defaultPageSizeWrapper: PrimitiveWrapper<Int>

) {
    private val defaultPageSize = defaultPageSizeWrapper.value


    fun getUser(id: Long) = api
            .getUser(id)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())


    fun followUser(
            userId: Long
    ) = api
            .followUser(
                    userId
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun unfollowUser(
            userId: Long
    ) = api
            .unfollowUser(
                    userId
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun getUserVideos(
            userId: Long,
            direction: Direction? = null,
            page: Int,
            pageSize: Int = defaultPageSize
    ) = api
            .getUsersVideos(
                    userId,
                    direction,
                    page,
                    pageSize
            )
            .map { t: VideoWrapper -> t.videoList }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
}