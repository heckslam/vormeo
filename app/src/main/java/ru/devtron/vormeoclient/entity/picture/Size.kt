package ru.devtron.vormeoclient.entity.picture

import com.google.gson.annotations.SerializedName

data class Size(
        @SerializedName("width") val width: Int,
        @SerializedName("height") val height: Int,
        @SerializedName("link") val link: String
)