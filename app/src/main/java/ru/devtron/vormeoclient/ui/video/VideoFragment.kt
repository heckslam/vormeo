package ru.devtron.vormeoclient.ui.video

import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.os.Bundle
import android.support.constraint.motion.MotionLayout
import android.support.v4.content.ContextCompat
import android.support.v4.media.AudioAttributesCompat
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.FileDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.*
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.fragment_video.*
import kotlinx.android.synthetic.main.playback_exo_control_view.*
import ru.devtron.vormeoclient.BuildConfig
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.extension.showSnackMessage
import ru.devtron.vormeoclient.presentation.video.VideoPresenter
import ru.devtron.vormeoclient.presentation.video.VideoView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import ru.devtron.vormeoclient.ui.video.player.AudioFocusExoPlayerDecorator
import ru.devtron.vormeoclient.ui.video.player.SimpleControlDispatcher
import timber.log.Timber
import toothpick.Toothpick
import java.io.File


/**
 * Created on 10.09.2018 by ruslanaliev.
 */
class VideoFragment : BaseFragment(), VideoView {
    override val layoutRes = R.layout.fragment_video
    private var player: ExoPlayer? = null
    private var simpleCache: Cache? = null
    private lateinit var cacheDataSourceFactory: CacheDataSourceFactory
    private lateinit var simpleControlDispatcher: SimpleControlDispatcher

    @InjectPresenter
    lateinit var presenter: VideoPresenter

    @ProvidePresenter
    fun providePresenter(): VideoPresenter =
            Toothpick
                    .openScope(DI.SERVER_SCOPE)
                    .getInstance(VideoPresenter::class.java)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        videoMotionLayout.addTransitionListener(object : MotionLayout.TransitionListener {
            override fun onTransitionChange(motionLayout: MotionLayout?, startId: Int, endId: Int, progress: Float) {
                presenter.onTransitionChange(progress)
            }

            override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
                Timber.d("onTransitionCompleted VideoFragment and progress was ${motionLayout?.progress} ")
                motionLayout?.progress?.let { progress ->
                    if (progress == 1f && !videoView.isControllerVisible) {
                        videoView.showController()
                    } else if (progress == 0f && videoView.isControllerVisible) {
                        videoView.hideController()
                    }
                }
            }
        })

        initPlayer()
        initVideoClicks()
    }

    private fun initVideoClicks() {
        exoArrowHideBtn.setOnClickListener {
            closeVideoAnim()
        }
    }

    private fun initPlayer() {
        val bandwidthMeter = DefaultBandwidthMeter()

        val audioManager = ContextCompat.getSystemService(requireContext(), AudioManager::class.java)!!

        val audioAttributes = AudioAttributesCompat.Builder()
                .setContentType(AudioAttributesCompat.CONTENT_TYPE_MOVIE)
                .setUsage(AudioAttributesCompat.USAGE_MEDIA)
                .build()

        player = AudioFocusExoPlayerDecorator(audioAttributes, audioManager,
                player = ExoPlayerFactory.newSimpleInstance(
                        requireContext(),
                        DefaultRenderersFactory(requireContext()),
                        DefaultTrackSelector(AdaptiveTrackSelection.Factory()),
                        DefaultLoadControl()
                ))

        val dataSourceFactory = DefaultDataSourceFactory(requireContext(), Util.getUserAgent(requireContext(), BuildConfig.APPLICATION_ID), bandwidthMeter)
        val maxCacheFileSize: Long = 1024 * 1024 * 2048L
        val file = File(requireContext().cacheDir, "video/")
        if (!SimpleCache.isCacheFolderLocked(file)) {
            simpleCache = SimpleCache(file, LeastRecentlyUsedCacheEvictor(maxCacheFileSize))
        } else {
            simpleCache?.release()
        }

        val cacheFlags = CacheDataSource.FLAG_BLOCK_ON_CACHE or
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR

        cacheDataSourceFactory = CacheDataSourceFactory(
                simpleCache,
                dataSourceFactory,
                FileDataSourceFactory(),
                CacheDataSinkFactory(simpleCache, maxCacheFileSize),
                cacheFlags,
                null
        )

        simpleControlDispatcher = SimpleControlDispatcher { url ->
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }

        player?.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                /*if (playbackState == Player.STATE_READY)
                    videoView.forceLayout()*/
            }
        })
        videoView.setControlDispatcher(simpleControlDispatcher)
        videoView.player = player
        videoView.hideController()
    }

    override fun showVideo(video: String) {
        player?.prepare(ExtractorMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(video)))

        player?.playWhenReady = true
    }

    override fun showMessage(msg: String) = showSnackMessage(msg)

    override fun openVideoAnim() {
        videoMotionLayout.transitionToEnd()
    }

    override fun closeVideoAnim() {
        videoMotionLayout.transitionToStart()
    }

    override fun pauseVideo() {
        player?.playWhenReady = false
    }

    override fun stopVideo() {
        player?.playWhenReady = false
        player?.release()
        player = null
        videoView.player = null
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter.onStop(player?.currentPosition, player?.playWhenReady ?: false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        stopVideo()
    }

    override fun restoreVideoPosition(currentVideoPosition: Long) {
        player?.seekTo(currentVideoPosition)
    }

    override fun restoreVideoState(playerIsReadyState: Boolean) {
        player?.playWhenReady = playerIsReadyState
    }
}
