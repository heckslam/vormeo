package ru.devtron.vormeoclient.model.data.auth

interface AuthHolder {
    var token: String?
    var serverPath: String
    var userId: Long
}