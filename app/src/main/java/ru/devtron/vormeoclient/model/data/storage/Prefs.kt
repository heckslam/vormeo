package ru.devtron.vormeoclient.model.data.storage

import android.content.Context
import ru.devtron.vormeoclient.di.qualifier.DefaultServerPath
import ru.devtron.vormeoclient.model.data.auth.AuthHolder
import javax.inject.Inject

class Prefs @Inject constructor(
        private val context: Context,
        @DefaultServerPath private val defaultServerPath: String
) : AuthHolder {

    private val AUTH_DATA = "auth_data"
    private val KEY_TOKEN = "ad_token"
    private val KEY_USER_ID = "user_id"
    private val KEY_SERVER_PATH = "ad_server_path"
    private fun getSharedPreferences(prefsName: String)
            = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

    override var token: String?
        get() = getSharedPreferences(AUTH_DATA).getString(KEY_TOKEN, null)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(KEY_TOKEN, value).apply()
        }

    override var userId: Long
        get() = getSharedPreferences(AUTH_DATA).getLong(KEY_USER_ID, 0)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putLong(KEY_USER_ID, value).apply()
        }

    override var serverPath: String
        get() = getSharedPreferences(AUTH_DATA).getString(KEY_SERVER_PATH, defaultServerPath)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(KEY_SERVER_PATH, value).apply()
        }
}