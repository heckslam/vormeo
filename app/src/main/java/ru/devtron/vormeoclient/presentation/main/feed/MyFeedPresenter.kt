package ru.devtron.vormeoclient.presentation.main.feed

import com.arellomobile.mvp.InjectViewState
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.entity.feed.FeedElement
import ru.devtron.vormeoclient.model.interactor.feed.FeedInteractor
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.global.*
import javax.inject.Inject

@InjectViewState
class MyFeedPresenter @Inject constructor(
        private val router: FlowRouter,
        private val feedInteractor: FeedInteractor,
        private val menuController: GlobalMenuController,
        private val errorHandler: ErrorHandler,
        private val globalVideoController: GlobalVideoController
) : BasePresenter<MyFeedView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refreshFeeds()
    }

    private val paginator = Paginator(
            { feedInteractor.getFeeds(it) },
            object : Paginator.ViewController<FeedElement> {
                override fun showEmptyProgress(show: Boolean) {
                    viewState.showEmptyProgress(show)
                }

                override fun showEmptyError(show: Boolean, error: Throwable?) {
                    if (error != null) {
                        errorHandler.proceed(error) { viewState.showEmptyError(show, it) }
                    } else {
                        viewState.showEmptyError(show, null)
                    }
                }

                override fun showErrorMessage(error: Throwable) {
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }

                override fun showEmptyView(show: Boolean) {
                    viewState.showEmptyView(show)
                }

                override fun showData(show: Boolean, data: List<FeedElement>) {
                    viewState.showFeed(show, data)
                }

                override fun showRefreshProgress(show: Boolean) {
                    viewState.showRefreshProgress(show)
                }

                override fun showPageProgress(show: Boolean) {
                    viewState.showPageProgress(show)
                }
            }
    )

    fun onMenuClick() = menuController.open()
    fun onFeedClick(feed: FeedElement) { globalVideoController.openVideo(feed.clip)}
    fun profileClick(userId: Long) = router.startFlow(Screens.USER_FLOW, userId)
    fun refreshFeeds() = paginator.refresh()
    fun loadNextFeedsPage() = paginator.loadNewPage()
    fun onBackPressed() = router.exit()

    override fun onDestroy() {
        super.onDestroy()
        paginator.release()
    }

}