package ru.devtron.vormeoclient.di.module

import ru.devtron.vormeoclient.presentation.global.GlobalMenuController
import toothpick.config.Module

class GlobalMenuModule : Module() {
    init {
        bind(GlobalMenuController::class.java).toInstance(GlobalMenuController())
    }
}