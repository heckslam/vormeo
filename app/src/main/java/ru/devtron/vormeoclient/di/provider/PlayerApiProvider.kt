package ru.devtron.vormeoclient.di.provider

import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.devtron.vormeoclient.model.data.server.BASE_URL
import ru.devtron.vormeoclient.model.data.server.VimeoPlayerApi
import javax.inject.Inject
import javax.inject.Provider


class PlayerApiProvider @Inject constructor(
        private val okHttpClient: OkHttpClient,
        private val gson: Gson
) : Provider<VimeoPlayerApi> {

    override fun get() =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(BASE_URL)
                    .build()
                    .create(VimeoPlayerApi::class.java)
}