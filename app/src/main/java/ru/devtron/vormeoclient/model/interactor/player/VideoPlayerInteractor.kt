package ru.devtron.vormeoclient.model.interactor.player

import io.reactivex.Single
import ru.devtron.vormeoclient.entity.player.Files
import ru.devtron.vormeoclient.entity.player.H264
import ru.devtron.vormeoclient.entity.player.ProgressiveData
import ru.devtron.vormeoclient.entity.player.Quality
import ru.devtron.vormeoclient.model.repository.player.VideoPlayerRepo
import javax.inject.Inject

/**
 * Created by ruslanaliev on 13.12.2017.
 */
class VideoPlayerInteractor @Inject constructor(
        private val libraryRepo: VideoPlayerRepo
) {
    fun getVideoUrl(videoId: String): Single<String> {
        return libraryRepo.getVideoConfig(videoId)
                .map { getVideoUrl(it.request.files) }
    }

    private fun getVideoUrl(files: Files): String {
        val progressiveDataUrl = files.progressive?.let { getProgressiveDataUrl(files.progressive) }
                ?: ""
        val h264VideoUrl = getH264OrVP6VideoUrl(files.h264)
        val vp6VideoUrl = getH264OrVP6VideoUrl(files.vp6)
        val hlsVideoUrl = files.hls?.url ?: ""

        return when {
            progressiveDataUrl.isNotEmpty() -> progressiveDataUrl
            h264VideoUrl.isNotEmpty() -> h264VideoUrl
            vp6VideoUrl.isNotEmpty() -> vp6VideoUrl
            hlsVideoUrl.isNotEmpty() -> hlsVideoUrl
            else -> ""
        }
    }

    private fun getProgressiveDataUrl(progressiveDataList: List<ProgressiveData>): String {
        for (progressiveData in progressiveDataList) {
            return when (progressiveData.quality) {
                Quality.FULLHD -> progressiveData.url
                Quality.HD -> progressiveData.url
                Quality.LOW -> progressiveData.url
            }
        }
        return ""
    }

    private fun getH264OrVP6VideoUrl(h264: H264?): String {
        return when {
            h264 == null -> ""
            h264.hd != null -> h264.hd.url
            h264.sd != null -> h264.sd.url
            h264.mobile != null -> h264.mobile.url
            else -> ""
        }
    }
}