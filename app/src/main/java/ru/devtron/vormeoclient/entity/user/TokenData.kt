package ru.devtron.vormeoclient.entity.user

import com.google.gson.annotations.SerializedName

data class TokenData(
        @SerializedName("access_token") val token: String,
        @SerializedName("token_type") val type: String,
        @SerializedName("scope") val scope: String,
        @SerializedName("user") val user: AuthorizedUser
)