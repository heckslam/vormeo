package ru.devtron.vormeoclient.presentation.main.feed

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devtron.vormeoclient.entity.feed.FeedElement

@StateStrategyType(AddToEndSingleStrategy::class)
interface MyFeedView : MvpView {
    fun showRefreshProgress(show: Boolean)
    fun showEmptyProgress(show: Boolean)
    fun showPageProgress(show: Boolean)
    fun showEmptyView(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showFeed(show: Boolean, feeds: List<FeedElement>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}