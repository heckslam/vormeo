package ru.devtron.vormeoclient.presentation.user

import com.arellomobile.mvp.MvpView
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import ru.terrakok.cicerone.Router
import toothpick.Scope
import toothpick.Toothpick
import javax.inject.Inject

/**
 * Created on 07.09.2018 by ruslanaliev.
 */
class UserFlowPresenter @Inject constructor(
        private val router: Router,
        private val scope: Scope
) : BasePresenter<MvpView>() {

    override fun onDestroy() {
        Toothpick.closeScope(scope.name)
        super.onDestroy()
    }

    fun onExit() {
        router.exit()
    }
}