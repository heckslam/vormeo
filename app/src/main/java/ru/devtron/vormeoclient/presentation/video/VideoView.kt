package ru.devtron.vormeoclient.presentation.video

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface VideoView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showVideo(video: String)

    fun openVideoAnim()
    fun closeVideoAnim()

    fun showMessage(msg: String)
    fun pauseVideo()
    fun stopVideo()
    fun restoreVideoPosition(currentVideoPosition: Long)
    fun restoreVideoState(playerIsReadyState: Boolean)
}