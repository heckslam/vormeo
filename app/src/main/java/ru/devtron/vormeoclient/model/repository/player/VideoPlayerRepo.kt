package ru.devtron.vormeoclient.model.repository.player

import ru.devtron.vormeoclient.model.data.server.VimeoPlayerApi
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import javax.inject.Inject

/**
 * Created on 11.09.2018 by ruslanaliev.
 */
class VideoPlayerRepo @Inject constructor(
        private val api: VimeoPlayerApi,
        private val schedulers: SchedulersProvider
) {
    fun getVideoConfig(
            videoId: String
    ) = api
            .getVideoConfig(
                    videoId
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
}