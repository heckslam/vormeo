package ru.devtron.vormeoclient.ui.main.library

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import kotlinx.android.synthetic.main.layout_base_list.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.LibraryState
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.extension.showSnackMessage
import ru.devtron.vormeoclient.extension.visible
import ru.devtron.vormeoclient.presentation.main.library.LibraryListPresenter
import ru.devtron.vormeoclient.presentation.main.library.LibraryView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import ru.devtron.vormeoclient.ui.global.list.LikeAdapterDelegate
import ru.devtron.vormeoclient.ui.global.list.ListItem
import ru.devtron.vormeoclient.ui.global.list.ProgressAdapterDelegate
import toothpick.Toothpick
import toothpick.config.Module

class LibraryListFragment : BaseFragment(), LibraryView {

    override val layoutRes = R.layout.fragment_follow

    private val adapter = LibraryAdapter()

    @InjectPresenter lateinit var presenter: LibraryListPresenter

    @ProvidePresenter
    fun providePresenter(): LibraryListPresenter {
        val scopeName = "LibraryListFragment${hashCode()}"
        val scope = Toothpick.openScopes(DI.DRAWER_FLOW_SCOPE, scopeName)
        scope.installModules(object : Module() {
            init {
                bind(PrimitiveWrapper::class.java)
                        .withName(LibraryState::class.java)
                        .toInstance(PrimitiveWrapper(arguments?.getInt(LIBRARY_STATE)))
            }
        })
        return scope.getInstance(LibraryListPresenter::class.java).also {
            Toothpick.closeScope(scopeName)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = this@LibraryListFragment.adapter
        }

        swipeToRefresh.setOnRefreshListener { presenter.refreshVideosRequests() }
    }

    override fun showRefreshProgress(show: Boolean) {
        swipeToRefresh.post { swipeToRefresh.isRefreshing = show }
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView.visible(show)

        //trick for disable and hide swipeToRefresh on fullscreen progress
        swipeToRefresh.visible(!show)
        swipeToRefresh.post { swipeToRefresh.isRefreshing = false }
    }

    override fun showLibrary(show: Boolean, videos: List<Video>) {
        recyclerView.visible(show)
        recyclerView.post { adapter.setData(videos) }
    }

    override fun showPageProgress(show: Boolean) {
        recyclerView.post { adapter.showProgress(show) }
    }

    override fun showEmptyView(show: Boolean) {
        //todo
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        //todo
        if (show && message != null) showSnackMessage(message)
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    inner class LibraryAdapter : ListDelegationAdapter<MutableList<ListItem>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(LikeAdapterDelegate({ presenter.onVideoClick(it) }, { presenter.profileClick(it) }))
            delegatesManager.addDelegate(ProgressAdapterDelegate())
        }

        fun setData(feeds: List<Video>) {
            val progress = isProgress()

            items.clear()
            items.addAll(feeds.map { ListItem.VideoItem(it) })
            if (progress) items.add(ListItem.ProgressItem())

            notifyDataSetChanged()
        }

        fun showProgress(isVisible: Boolean) {
            val currentProgress = isProgress()

            if (isVisible && !currentProgress) items.add(ListItem.ProgressItem())
            else if (!isVisible && currentProgress) items.remove(items.last())

            notifyDataSetChanged()
        }

        private fun isProgress() = items.isNotEmpty() && items.last() is ListItem.ProgressItem

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any?>) {
            super.onBindViewHolder(holder, position, payloads)
            if (position == items.size - 10) presenter.loadNextPage()
        }
    }

    companion object {
        private const val LIBRARY_STATE = "LIBRARY_STATE"
        const val LIBRARY_STATE_WATCH_LATER = 0
        const val LIBRARY_STATE_LIKES = 1
        fun createNewInstance(libraryState: Int) = LibraryListFragment().apply {
            arguments = Bundle().apply {
                putInt(LIBRARY_STATE, libraryState)
            }
        }
    }
}