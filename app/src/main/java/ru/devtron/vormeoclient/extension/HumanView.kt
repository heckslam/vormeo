package ru.devtron.vormeoclient.extension

import android.content.res.Resources
import android.support.annotation.DrawableRes
import org.threeten.bp.Duration
import org.threeten.bp.LocalDateTime
import retrofit2.HttpException
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.entity.user.AccountType
import ru.devtron.vormeoclient.model.system.ResourceManager
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created on 06.09.2018 by ruslanaliev.
 */

fun Throwable.userMessage(resourceManager: ResourceManager) = when (this) {
    is HttpException -> when (this.code()) {
        304 -> resourceManager.getString(R.string.not_modified_error)
        400 -> resourceManager.getString(R.string.bad_request_error)
        401 -> resourceManager.getString(R.string.unauthorized_error)
        403 -> resourceManager.getString(R.string.forbidden_error)
        404 -> resourceManager.getString(R.string.not_found_error)
        405 -> resourceManager.getString(R.string.method_not_allowed_error)
        409 -> resourceManager.getString(R.string.conflict_error)
        422 -> resourceManager.getString(R.string.unprocessable_error)
        500 -> resourceManager.getString(R.string.server_error_error)
        else -> resourceManager.getString(R.string.unknown_error)
    }
    is IOException -> resourceManager.getString(R.string.network_error)
    else -> resourceManager.getString(R.string.unknown_error)
}

fun LocalDateTime.humanTime(resources: Resources): String {
    val timeDelta = Duration.between(this, LocalDateTime.now()).seconds

    val timeStr =
            when {
                timeDelta < 60 -> resources.getString(R.string.time_sec, timeDelta)
                timeDelta < 60 * 60 -> resources.getString(R.string.time_min, timeDelta / 60)
                timeDelta < 60 * 60 * 24 -> {
                    val time = (timeDelta / (60 * 60)).toInt()
                    resources.getQuantityString(R.plurals.time_hours, time, time)
                }
                timeDelta < 60 * 60 * 24 * 7 -> {
                    val time = (timeDelta / (60 * 60 * 24)).toInt()
                    resources.getQuantityString(R.plurals.time_days, time, time)
                }
                else -> return this.toLocalDate().toString()
            }

    return resources.getString(R.string.time_ago, timeStr)
}


private val YEAR_DATE_FORMAT = SimpleDateFormat("yyyy", Locale.getDefault())
fun Date.humanYear(resources: Resources): String {
    return resources.getString(R.string.since_year, YEAR_DATE_FORMAT.format(this))
}

fun Long.formatDuration(duration: Long): String {
    val minutes = (duration / 60)
    val seconds = (duration % 60)

    val formattedDuration: String
    if (minutes == 0L) {
        formattedDuration = if (seconds > 0L) {
            if (seconds < 10L)
                String.format("0:0%s", seconds.toString())
            else
                String.format("0:%s", seconds.toString())
        } else {
            "0:00"
        }
    } else {
        formattedDuration = if (seconds > 0L) {
            if (seconds < 10L)
                String.format("%s:0%s", minutes.toString(), seconds.toString())
            else
                String.format("%s:%s", minutes.toString(), seconds.toString())
        } else {
            String.format("%s:00", minutes.toString())
        }
    }
    return formattedDuration
}

fun Int.humanViewCount(res: Resources): String {
    val formattedViewCountWithPostfix = when (this) {
        in 1000000..999999999 -> "${this / 1000000}M"
        in 1000..999999 -> "${this / 1000}K"
        in 1..999 -> "$this"
        else -> "0"
    }
    return res.getQuantityString(R.plurals.views_count, this, formattedViewCountWithPostfix)
}

@DrawableRes
fun AccountType.getIcon() = when (this) {
    AccountType.BASIC -> R.drawable.ic_free
    AccountType.PREMIUM -> R.drawable.ic_premium_profile
    AccountType.PRO -> R.drawable.ic_premium_profile
}