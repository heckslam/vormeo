package ru.devtron.vormeoclient.entity.player

import com.google.gson.annotations.SerializedName

data class VideoConfig(
        val request: Request
)

data class Request(
        val files: Files
)

data class Files(
        val h264: H264?,
        val hls: HLS?,
        val vp6: H264?,
        val progressive: List<ProgressiveData>?
)

data class H264(
        val sd: VideoFormat?,
        val hd: VideoFormat?,
        val mobile: VideoFormat?
)

data class HLS(
        val url: String
)

data class VideoFormat(
        val url: String,
        val width: Int,
        val height: Int
)

data class ProgressiveData(
        val url: String,
        val quality: Quality
)

enum class Quality {
    @SerializedName("360p") LOW,
    @SerializedName("720p") HD,
    @SerializedName("1080p") FULLHD
}
