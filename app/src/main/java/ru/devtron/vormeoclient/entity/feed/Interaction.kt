package ru.devtron.vormeoclient.entity.feed

import com.google.gson.annotations.SerializedName
import java.util.*

data class Interaction (
    @SerializedName("added") val added: Boolean,
    @SerializedName("added_time") val addedTime: Date,
    @SerializedName("uri") val uri: String
)