package ru.devtron.vormeoclient.presentation.drawer

import com.arellomobile.mvp.InjectViewState
import io.reactivex.rxkotlin.plusAssign
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.model.interactor.auth.AuthInteractor
import ru.devtron.vormeoclient.model.interactor.profile.MyProfileInteractor
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.drawer.NavigationDrawerView.MenuItem
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import ru.devtron.vormeoclient.presentation.global.GlobalMenuController
import ru.devtron.vormeoclient.presentation.global.GlobalVideoController
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class NavigationDrawerPresenter @Inject constructor(
        private val router: FlowRouter,
        private val menuController: GlobalMenuController,
        private val authInteractor: AuthInteractor,
        private val myProfileInteractor: MyProfileInteractor,
        private val globalVideoController: GlobalVideoController
) : BasePresenter<NavigationDrawerView>() {

    private var currentSelectedItem: NavigationDrawerView.MenuItem? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        subscribeOnProfileUpdates()
    }

    private fun subscribeOnProfileUpdates() {
        disposables += myProfileInteractor.getMyProfile()
                .subscribe(
                        { viewState.showUserInfo(it) },
                        { error -> Timber.d("getMyProfile(): $error") }
                )
    }

    fun onScreenChanged(item: NavigationDrawerView.MenuItem) {
        menuController.close()
        currentSelectedItem = item
        viewState.selectMenuItem(item)
    }

    fun onMenuItemClick(item: MenuItem) {
        menuController.close()
        globalVideoController.dropVideo()
        if (item != currentSelectedItem) {
            when (item) {
                MenuItem.WATCH_NOW -> router.newRootScreen(Screens.MAIN_FLOW)
                MenuItem.ABOUT -> router.newRootScreen(Screens.ABOUT_SCREEN)
                MenuItem.PROFILE -> router.startFlow(Screens.USER_FLOW, authInteractor.userId)
                MenuItem.FEEDBACK -> viewState.showRatingDialog()
            }
        }
    }

    fun onLogoutClick() {
        menuController.close()
        authInteractor.logout()
        router.newRootFlow(Screens.AUTH_FLOW)
    }

    fun onUserClick(id: Long) {
        menuController.close()
        router.startFlow(Screens.USER_FLOW, id)
    }
}