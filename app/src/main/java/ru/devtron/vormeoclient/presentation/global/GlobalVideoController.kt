package ru.devtron.vormeoclient.presentation.global

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.model.system.None
import ru.devtron.vormeoclient.model.system.Optional
import ru.devtron.vormeoclient.model.system.Some

/**
 * Created on 11.09.2018 by ruslanaliev.
 */
class GlobalVideoController {
    private val stateRelay = PublishRelay.create<Optional<Video>>()
    val progressAnim = BehaviorRelay.create<Float>()

    val state: Observable<Optional<Video>> = stateRelay
    val progressAnimState: Observable<Float> = progressAnim
    fun openVideo(video: Video) = stateRelay.accept(Some(video))
    fun dropVideo() = stateRelay.accept(None)

    fun updateProgressAnim(progress: Float) = progressAnim.accept(progress)
}