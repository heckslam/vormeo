package ru.devtron.vormeoclient.presentation.user

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.entity.video.Video

@StateStrategyType(AddToEndSingleStrategy::class)
interface UserInfoView : MvpView {
    fun showUser(user: AuthorizedUserMe)
    fun showProgress(show: Boolean)
    fun isFollowing(following: Boolean)

    fun showRefreshProgress(show: Boolean)
    fun showEmptyProgress(show: Boolean)
    fun showPageProgress(show: Boolean)
    fun showEmptyView(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showVideos(show: Boolean, videos: List<Video>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(msg: String)
}