package ru.devtron.vormeoclient.entity.user

import com.google.gson.annotations.SerializedName

/**
 * Created by ruslanaliev on 12.12.2017.
 */
enum class AccountType(private val jsonName: String) {
    @SerializedName("basic") BASIC("basic"),
    @SerializedName("plus") PREMIUM("plus"),
    @SerializedName("pro") PRO("pro");

    override fun toString() = jsonName
}