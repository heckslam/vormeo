package ru.devtron.vormeoclient.ui.user.info

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_user_info.*
import kotlinx.android.synthetic.main.layout_avatar.*
import kotlinx.android.synthetic.main.toolbar_profile.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.extension.*
import ru.devtron.vormeoclient.presentation.user.UserInfoPresenter
import ru.devtron.vormeoclient.presentation.user.UserInfoView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import ru.devtron.vormeoclient.ui.global.VideoAdapter
import ru.devtron.vormeoclient.ui.main.followers.FollowersFragment
import toothpick.Toothpick

class UserInfoFragment : BaseFragment(), UserInfoView {

    private val adapter = VideoAdapter({ presenter.onVideoClick(it) }, { presenter.profileClick(it) }, { presenter.loadNextLikesPage() })

    override val layoutRes = R.layout.fragment_user_info
    private var user: AuthorizedUserMe? = null

    private val scopeName: String? by argument(ARG_SCOPE_NAME)

    @InjectPresenter
    lateinit var presenter: UserInfoPresenter

    @ProvidePresenter
    fun providePresenter(): UserInfoPresenter =
            Toothpick
                    .openScope(scopeName)
                    .getInstance(UserInfoPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }

        toolbar.inflateMenu(R.menu.share_menu)
        toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.shareAction -> shareText(user?.link)
            }
            true
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = this@UserInfoFragment.adapter
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }

    override fun showUser(user: AuthorizedUserMe) {
        this.user = user
        toolbar.title = user.name
        letterTV.text = user.name.first().toString().toUpperCase()
        avatarIV.loadRoundedImage(user.avatarUrl)
        usernameTextView.text = user.name
        locationTv.showTextOrHide(user.location)
        tvLikes.text = "${user.metadata.connections.likes.total}"
        tvFollowers.text = "${user.metadata.connections.followers.total}"
        tvFollowing.text = "${user.metadata.connections.following.total}"

        user.accountType?.getIcon()?.let { userAccountTypeImageView.setImageResource(it) }

        if (user.metadata.interactions == null) {
            followBtn.visibility = View.GONE
        } else {
            followBtn.isChecked = user.metadata.interactions?.follow?.added ?: false
        }

        followBtn.setOnClickListener { presenter.onFollowClick(!followBtn.isChecked) }
        followersContainer.setOnClickListener { presenter.showFollowers(FollowersFragment.ARG_FOLLOWERS, scopeName) }
        followingContainer.setOnClickListener { presenter.showFollowers(FollowersFragment.ARG_FOLLOWING, scopeName) }
        likesContainer.setOnClickListener { presenter.showLikes(scopeName) }


        userBioTextView.showTextOrHide(user.bio)
        locationTv.showTextOrHide(user.location)
    }

    override fun showProgress(show: Boolean) = showProgressDialog(show)

    override fun showMessage(msg: String) = showSnackMessage(msg)

    override fun isFollowing(following: Boolean) {
        followBtn.isChecked = following
    }

    override fun showRefreshProgress(show: Boolean) { fullscreenProgressView.visible(show) }

    override fun showEmptyProgress(show: Boolean) { fullscreenProgressView.visible(show) }

    override fun showPageProgress(show: Boolean) { recyclerView.post { adapter.showProgress(show) } }

    override fun showEmptyView(show: Boolean) {}

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show && message != null) showSnackMessage(message)
    }

    override fun showVideos(show: Boolean, videos: List<Video>) {
        recyclerView.visible(show)
        recyclerView.post { adapter.setData(videos) }
    }

    companion object {
        private const val ARG_SCOPE_NAME = "arg_scope_name"
        fun create(scope: String) =
                UserInfoFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_SCOPE_NAME, scope)
                    }
                }
    }
}