package ru.devtron.vormeoclient.model.interactor

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import ru.devtron.vormeoclient.entity.user.AuthorizedUser
import ru.devtron.vormeoclient.entity.user.TokenData
import ru.devtron.vormeoclient.model.interactor.auth.AuthInteractor
import ru.devtron.vormeoclient.model.interactor.auth.OAuthParams
import ru.devtron.vormeoclient.model.repository.auth.AuthRepository
import java.util.*
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.plugins.RxAndroidPlugins



/**
 * Created by ruslanaliev on 07.02.2018.
 */
class AuthInteractorTest {

    private lateinit var interactor: AuthInteractor
    private lateinit var authRepo: AuthRepository

    private val HASH = "some_hash_here"
    private val OAUTH_PARAMS = OAuthParams("appId", "appKey", "redirect_url", "scope")

    @Before
    fun setUp() {
        authRepo = mock(AuthRepository::class.java)
        interactor = AuthInteractor("some server path", authRepo, HASH, OAUTH_PARAMS)
    }

    @Test
    fun check_oauth_redirect() {
        val testUrl = OAUTH_PARAMS.redirectUrl + "somepath"
        val result = interactor.checkOAuthRedirect(testUrl)
        assertTrue(result)
    }

    @Test
    fun check_oauth_bad_redirect_path() {
        val testUrl = "app://otherUrl/somepath"
        val result = interactor.checkOAuthRedirect(testUrl)
        assertFalse(result)
    }

    @Test
    fun check_logout_cleans_token() {
        interactor.logout()
        verify(authRepo).clearAuthData()
    }

    @Test
    fun is_signed_in() {
        `when`(authRepo.isSignedIn).thenReturn(true)
        val result = interactor.isSignedIn()

        verify(authRepo).isSignedIn
        assertTrue(result)
    }

    @Test
    fun is_not_signed_in() {
        `when`(authRepo.isSignedIn).thenReturn(false)
        val result = interactor.isSignedIn()

        verify(authRepo).isSignedIn
        assertFalse(result)
    }

    //TODO: Write test for testing correct oauth

    @Test
    fun refresh_token_incorrect_oauth() {
        val testOauthRedirect = "There is no token"

        val testObserver: TestObserver<Void> = interactor.login(testOauthRedirect).test()
        testObserver.awaitTerminalEvent()

        verifyNoMoreInteractions(authRepo)

        testObserver
                .assertNoValues()
                .assertError(RuntimeException::class.java)
    }
}