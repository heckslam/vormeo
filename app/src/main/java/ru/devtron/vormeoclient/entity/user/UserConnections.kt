package ru.devtron.vormeoclient.entity.user

import com.google.gson.annotations.SerializedName
import ru.devtron.vormeoclient.entity.feed.Connection

/**
 * Created by ruslanaliev on 11.12.2017.
 */
data class UserConnections (
        @SerializedName("followers") val followers: Connection,
        @SerializedName("following") val following: Connection,
        @SerializedName("likes") val likes: Connection,
        @SerializedName("videos") val videos: Connection
)