package ru.devtron.vormeoclient.presentation.global

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable

/**
 * Created on 06.09.2018 by ruslanaliev.
 */
open class BasePresenter<V : MvpView> : MvpPresenter<V>() {

    protected val disposables = CompositeDisposable()

    override fun onDestroy() {
        disposables.dispose()
    }
}