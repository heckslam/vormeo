package ru.devtron.vormeoclient.model.interactor

import io.reactivex.Single
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.model.repository.LibraryRepository
import ru.devtron.vormeoclient.ui.main.followers.FollowersFragment
import javax.inject.Inject

/**
 * Created by ruslanaliev on 13.12.2017.
 */
class LibraryListInteractor @Inject constructor(
        private val libraryRepo: LibraryRepository
) {
    fun getVideos(
            state: Int,
            page: Int
    ): Single<List<Video>> {
        return if (state == FollowersFragment.ARG_FOLLOWERS) {
            libraryRepo
                    .getWatchLater(
                            page = page
                    )
        } else {
            libraryRepo
                    .getLikes(
                            page = page
                    )
        }
    }
}