package ru.devtron.vormeoclient.ui.global.glide

import android.app.ActivityManager
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.DecodeFormat.PREFER_ARGB_8888
import com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions


/**
 * Created on 14.05.2018 by ruslanaliev.
 */
/**
 * Glide module configurations
 */
@GlideModule
class VormeoGlideModule : AppGlideModule() {

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        val activityManager = context.getSystemService(ACTIVITY_SERVICE) as ActivityManager
        // Prefer higher quality images unless we're on a low RAM device
        // Disable hardware bitmaps as they don't play nicely with Palette
        val defaultOptions = RequestOptions()
                .format(if (activityManager.isLowRamDevice) PREFER_RGB_565 else PREFER_ARGB_8888)
                .disallowHardwareConfig()
        builder.setDefaultRequestOptions(defaultOptions)
    }

    override fun isManifestParsingEnabled(): Boolean {
        return false
    }
}