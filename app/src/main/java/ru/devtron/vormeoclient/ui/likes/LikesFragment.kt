package ru.devtron.vormeoclient.ui.likes

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_my_likes.*
import kotlinx.android.synthetic.main.layout_base_list.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.UserId
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.extension.argument
import ru.devtron.vormeoclient.extension.showSnackMessage
import ru.devtron.vormeoclient.extension.visible
import ru.devtron.vormeoclient.presentation.likes.LikesPresenter
import ru.devtron.vormeoclient.presentation.likes.LikesView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import ru.devtron.vormeoclient.ui.global.VideoAdapter
import toothpick.Toothpick
import toothpick.config.Module

class LikesFragment : BaseFragment(), LikesView {

    override val layoutRes = R.layout.fragment_my_likes

    private val adapter = VideoAdapter({ presenter.onVideoClick(it) }, { presenter.profileClick(it) }, { presenter.loadNextLikesPage() })
    private val scopeName: String? by argument(ARG_SCOPE_NAME)

    @InjectPresenter
    lateinit var presenter: LikesPresenter

    @ProvidePresenter
    fun providePresenter(): LikesPresenter {
        val subScopeName = "LikesFragment${hashCode()}"
        val scope = Toothpick.openScopes(scopeName, subScopeName)
        scope.installModules(object : Module() {
            init {
                bind(PrimitiveWrapper::class.java)
                        .withName(UserId::class.java)
                        .toInstance(PrimitiveWrapper(arguments?.getLong(LikesFragment.ARG_USER_ID)))
            }
        })
        return scope.getInstance(LikesPresenter::class.java).also {
            Toothpick.closeScope(scopeName)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = this@LikesFragment.adapter
        }

        swipeToRefresh.setOnRefreshListener { presenter.refreshLikes() }
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
    }

    override fun onBackPressed() = presenter.onBackPressed()

    override fun showRefreshProgress(show: Boolean) {
        swipeToRefresh.post { swipeToRefresh.isRefreshing = show }
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView.visible(show)

        //trick for disable and hide swipeToRefresh on fullscreen progress
        swipeToRefresh.visible(!show)
        swipeToRefresh.post { swipeToRefresh.isRefreshing = false }
    }

    override fun showVideos(show: Boolean, videos: List<Video>) {
        recyclerView.visible(show)
        recyclerView.post { adapter.setData(videos) }
    }

    override fun showPageProgress(show: Boolean) {
        recyclerView.post { adapter.showProgress(show) }
    }

    override fun showEmptyView(show: Boolean) {
        //todo
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        //todo
        if (show && message != null) showSnackMessage(message)
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    companion object {
        private const val ARG_USER_ID = "arg_user_id"
        private const val ARG_SCOPE_NAME = "arg_scope_name"
        fun createNewInstance(userId: Long, scope: String) = LikesFragment().apply {
            arguments = Bundle().apply {
                putLong(ARG_USER_ID, userId)
                putString(ARG_SCOPE_NAME, scope)
            }
        }
    }
}