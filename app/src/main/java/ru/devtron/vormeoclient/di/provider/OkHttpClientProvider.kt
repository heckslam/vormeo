package ru.devtron.vormeoclient.di.provider

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import ru.devtron.vormeoclient.BuildConfig
import ru.devtron.vormeoclient.model.data.auth.AuthHolder
import ru.devtron.vormeoclient.model.data.server.interceptor.AuthHeaderInterceptor
import ru.devtron.vormeoclient.model.data.server.interceptor.CurlLoggingInterceptor
import ru.devtron.vormeoclient.model.data.server.interceptor.ErrorResponseInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider


class OkHttpClientProvider @Inject constructor(authData: AuthHolder) : Provider<OkHttpClient> {
    private val httpClient: OkHttpClient

    init {
        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.addNetworkInterceptor(AuthHeaderInterceptor(authData))
        httpClientBuilder.addNetworkInterceptor(ErrorResponseInterceptor())
        httpClientBuilder.readTimeout(30, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addNetworkInterceptor(httpLoggingInterceptor)
            //httpClientBuilder.addNetworkInterceptor(CurlLoggingInterceptor())
        }
        httpClient = httpClientBuilder.build()
    }

    override fun get() = httpClient
}