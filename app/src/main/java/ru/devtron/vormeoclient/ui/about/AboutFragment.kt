package ru.devtron.vormeoclient.ui.about

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_about.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.presentation.about.AboutPresenter
import ru.devtron.vormeoclient.presentation.about.AboutView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import toothpick.Toothpick

class AboutFragment : BaseFragment(), AboutView {
    override val layoutRes = R.layout.fragment_about

    @InjectPresenter lateinit var presenter: AboutPresenter

    @ProvidePresenter
    fun providePresenter(): AboutPresenter {
        return Toothpick
                .openScope(DI.DRAWER_FLOW_SCOPE)
                .getInstance(AboutPresenter::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.setNavigationOnClickListener { presenter.onMenuPressed() }
    }

    override fun showAppVersion(version: String) {
        aboutVersionTV.text = version
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}