package ru.devtron.vormeoclient.di.module

import ru.devtron.vormeoclient.model.system.flow.AppRouter
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import toothpick.config.Module

class FlowNavigationModule(globalRouter: AppRouter) : Module() {
    init {
        val cicerone = Cicerone.create(FlowRouter(globalRouter))
        bind(FlowRouter::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)
    }
}