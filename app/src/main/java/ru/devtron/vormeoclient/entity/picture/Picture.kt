package ru.devtron.vormeoclient.entity.picture

import com.google.gson.annotations.SerializedName

data class Picture(
        @SerializedName("type") val type: String,
        @SerializedName("width") val width: Int,
        @SerializedName("height") val height: Int,
        @SerializedName("link") val link: String
)