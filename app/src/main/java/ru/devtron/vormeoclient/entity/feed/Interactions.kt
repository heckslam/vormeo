package ru.devtron.vormeoclient.entity.feed

import com.google.gson.annotations.SerializedName

data class Interactions (
    @SerializedName("watchlater") val watchlater: Interaction,
    @SerializedName("like") val like: Interaction
)