package ru.devtron.vormeoclient.ui.main.followers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import kotlinx.android.synthetic.main.layout_base_list.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.FollowersState
import ru.devtron.vormeoclient.di.qualifier.UserId
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.extension.showSnackMessage
import ru.devtron.vormeoclient.extension.visible
import ru.devtron.vormeoclient.presentation.main.followers.FollowersListPresenter
import ru.devtron.vormeoclient.presentation.main.followers.FollowersView
import ru.devtron.vormeoclient.ui.global.BaseFragment
import ru.devtron.vormeoclient.ui.global.list.FollowersAdapterDelegate
import ru.devtron.vormeoclient.ui.global.list.ListItem
import ru.devtron.vormeoclient.ui.global.list.ProgressAdapterDelegate
import toothpick.Toothpick
import toothpick.config.Module

/**
 * Created by ruslanaliev on 12.12.2017.
 */
class FollowersFragment : BaseFragment(), FollowersView {

    override val layoutRes = R.layout.fragment_follow
    private val adapter = FollowersListAdapter()

    @InjectPresenter lateinit var presenter: FollowersListPresenter

    @ProvidePresenter
    fun providePresenter(): FollowersListPresenter {
        val scopeName = "FollowersListScope_${hashCode()}"
        val scope = Toothpick.openScopes(DI.DRAWER_FLOW_SCOPE, scopeName)
        scope.installModules(object : Module() {
            init {
                bind(PrimitiveWrapper::class.java)
                        .withName(FollowersState::class.java)
                        .toInstance(PrimitiveWrapper(arguments?.getInt(FollowersFragment.ARG_STATE)))
                bind(PrimitiveWrapper::class.java)
                        .withName(UserId::class.java)
                        .toInstance(PrimitiveWrapper(arguments?.getLong(FollowersFragment.ARG_USER_ID)))
            }
        })

        return scope.getInstance(FollowersListPresenter::class.java).also {
            Toothpick.closeScope(scopeName)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@FollowersFragment.adapter
        }

        swipeToRefresh.setOnRefreshListener { presenter.refreshFollowerRequests() }
    }

    override fun showRefreshProgress(show: Boolean) {
        swipeToRefresh.post { swipeToRefresh.isRefreshing = show }
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView.visible(show)

        //trick for disable and hide swipeToRefresh on fullscreen progress
        swipeToRefresh.visible(!show)
        swipeToRefresh.post { swipeToRefresh.isRefreshing = false }
    }

    override fun showPageProgress(show: Boolean) {
        recyclerView.post { adapter.showProgress(show) }
    }

    override fun showEmptyView(show: Boolean) {
        //todo
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        //todo
        if (show && message != null) showSnackMessage(message)
    }

    override fun showFollowers(show: Boolean, followers: List<AuthorizedUserMe>) {
        recyclerView.visible(show)
        recyclerView.post { adapter.setData(followers) }
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    private inner class FollowersListAdapter : ListDelegationAdapter<MutableList<ListItem>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(FollowersAdapterDelegate { presenter.onFollowerRequestClick(it) })
            delegatesManager.addDelegate(ProgressAdapterDelegate())
        }

        fun setData(followers: List<AuthorizedUserMe>) {
            val progress = isProgress()

            items.clear()
            items.addAll(followers.map { ListItem.FollowerItem(it) })
            if (progress) items.add(ListItem.ProgressItem())

            notifyDataSetChanged()
        }

        fun showProgress(isVisible: Boolean) {
            val currentProgress = isProgress()

            if (isVisible && !currentProgress) items.add(ListItem.ProgressItem())
            else if (!isVisible && currentProgress) items.remove(items.last())

            notifyDataSetChanged()
        }

        private fun isProgress() = items.isNotEmpty() && items.last() is ListItem.ProgressItem

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any?>) {
            super.onBindViewHolder(holder, position, payloads)
            if (position == items.size - 10) presenter.loadNextFollowerRequestsPage()
        }
    }

    companion object {
        private const val ARG_STATE = "arg_state"
        private const val ARG_USER_ID = "arg_user_id"
        const val ARG_FOLLOWERS = 0
        const val ARG_FOLLOWING = 1

        fun newInstance(state: Int, userId: Long) = FollowersFragment().apply {
            arguments = Bundle().apply {
                putInt(ARG_STATE, state)
                putLong(ARG_USER_ID, userId)
            }
        }
    }
}