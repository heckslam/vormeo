package ru.devtron.vormeoclient.entity.feed

import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalDateTime
import ru.devtron.vormeoclient.entity.video.Video

data class FeedElement(
        @SerializedName("uri") val uri: Any,
        @SerializedName("clip") val clip: Video,
        @SerializedName("type") val type: String,
        @SerializedName("time") val time: LocalDateTime,
        @SerializedName("metadata") val metaInfo: MetaInfo,
        @SerializedName("channel") val channel: Channel?
)