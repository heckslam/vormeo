package ru.devtron.vormeoclient.presentation.main.library

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.LibraryState
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.model.interactor.LibraryListInteractor
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.global.ErrorHandler
import ru.devtron.vormeoclient.presentation.global.GlobalVideoController
import ru.devtron.vormeoclient.presentation.global.Paginator
import javax.inject.Inject

/**
 * Created by ruslanaliev on 12.12.2017.
 */
@InjectViewState
class LibraryListPresenter @Inject constructor(
        @LibraryState val libraryState: PrimitiveWrapper<Int>,
        private val router: FlowRouter,
        private val interactor: LibraryListInteractor,
        private val errorHandler: ErrorHandler,
        private val globalVideoController: GlobalVideoController
) : MvpPresenter<LibraryView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refreshVideosRequests()
    }

    private val paginator = Paginator(
            { interactor.getVideos(libraryState.value, it) },
            object : Paginator.ViewController<Video> {
                override fun showData(show: Boolean, data: List<Video>) {
                    viewState.showLibrary(show, data)
                }

                override fun showEmptyProgress(show: Boolean) {
                    viewState.showEmptyProgress(show)
                }

                override fun showEmptyError(show: Boolean, error: Throwable?) {
                    if (error != null) {
                        errorHandler.proceed(error) { viewState.showEmptyError(show, it) }
                    } else {
                        viewState.showEmptyError(show, null)
                    }
                }

                override fun showErrorMessage(error: Throwable) {
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }

                override fun showEmptyView(show: Boolean) {
                    viewState.showEmptyView(show)
                }

                override fun showRefreshProgress(show: Boolean) {
                    viewState.showRefreshProgress(show)
                }

                override fun showPageProgress(show: Boolean) {
                    viewState.showPageProgress(show)
                }
            }
    )

    fun onVideoClick(video: Video) { globalVideoController.openVideo(video) }
    fun profileClick(userId: Long) = router.startFlow(Screens.USER_FLOW, userId)
    fun refreshVideosRequests() = paginator.refresh()
    fun loadNextPage() = paginator.loadNewPage()

    override fun onDestroy() {
        super.onDestroy()
        paginator.release()
    }
}