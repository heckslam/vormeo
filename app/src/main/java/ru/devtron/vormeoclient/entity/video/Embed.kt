package ru.devtron.vormeoclient.entity.video

import com.google.gson.annotations.SerializedName

data class Embed (@SerializedName("html") val html: String)