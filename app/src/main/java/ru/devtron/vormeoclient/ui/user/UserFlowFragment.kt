package ru.devtron.vormeoclient.ui.user

import android.os.Bundle
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.module.FlowNavigationModule
import ru.devtron.vormeoclient.di.qualifier.UserId
import ru.devtron.vormeoclient.extension.argument
import ru.devtron.vormeoclient.extension.setLaunchScreen
import ru.devtron.vormeoclient.model.system.flow.AppRouter
import ru.devtron.vormeoclient.presentation.user.UserFlowPresenter
import ru.devtron.vormeoclient.ui.global.FlowFragment
import toothpick.Toothpick
import toothpick.config.Module

/**
 * Created on 07.09.2018 by ruslanaliev.
 */
class UserFlowFragment : FlowFragment(), MvpView {

    private val userId by argument(ARG_USER_ID, 0L)
    private val scopeName: String? by argument(ARG_SCOPE_NAME)

    @InjectPresenter
    lateinit var presenter: UserFlowPresenter

    @ProvidePresenter
    fun providePresenter() =
            Toothpick.openScope(scopeName)
                    .getInstance(UserFlowPresenter::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        initScope()
        super.onCreate(savedInstanceState)
        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.USER_INFO_SCREEN, scopeName)
        }
    }

    private fun initScope() {
        val scope = Toothpick.openScopes(DI.SERVER_SCOPE, scopeName)
        scope.installModules(
                FlowNavigationModule(scope.getInstance(AppRouter::class.java)),
                object : Module() {
                    init {
                        bind(PrimitiveWrapper::class.java)
                                .withName(UserId::class.java)
                                .toInstance(PrimitiveWrapper(userId))
                    }
                }
        )
        Toothpick.inject(this, scope)
    }

    override fun onExit() {
        presenter.onExit()
    }

    companion object {
        private const val ARG_USER_ID = "arg_user_id"
        private const val ARG_SCOPE_NAME = "arg_scope_name"
        fun create(userId: Long, scope: String) =
                UserFlowFragment().apply {
                    arguments = Bundle().apply {
                        putLong(ARG_USER_ID, userId)
                        putString(ARG_SCOPE_NAME, scope)
                    }
                }
    }
}