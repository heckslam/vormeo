package ru.devtron.vormeoclient.model.data.server

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*
import ru.devtron.vormeoclient.entity.Direction
import ru.devtron.vormeoclient.entity.Feed
import ru.devtron.vormeoclient.entity.VideoWrapper
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.entity.user.Followers
import ru.devtron.vormeoclient.entity.user.TokenData


interface VimeoApi {

    @FormUrlEncoded
    @POST("oauth/access_token")
    fun auth(
            @Field("code") code: String,
            @Field("redirect_uri") redirectUri: String,
            @Field("grant_type") type: String = "authorization_code"
    ): Single<TokenData>

    @GET("me")
    fun getMyUser(): Single<AuthorizedUserMe>

    @GET("me/feed")
    fun getFeed(@Query("page") page: Int,
                @Query("per_page") perPage: Int): Single<Feed>


    @GET("users/{user_id}")
    fun getUser(@Path("user_id") userId: Long): Single<AuthorizedUserMe>

    @GET("users/{user_id}/following")
    fun getFollowing(@Path("user_id") userId: Long,
                     @Query("page") page: Int,
                     @Query("per_page") perPage: Int): Single<Followers>

    @GET("users/{user_id}/followers")
    fun getFollowers(@Path("user_id") userId: Long,
                     @Query("page") page: Int,
                     @Query("per_page") perPage: Int): Single<Followers>

    @PUT("me/following/{follow_user_id}")
    fun followUser(@Path("follow_user_id") userId: Long): Completable

    @DELETE("me/following/{follow_user_id}")
    fun unfollowUser(@Path("follow_user_id") userId: Long): Completable

    @GET("users/{user_id}/likes")
    fun getLikes(@Path("user_id") userId: Long,
                 @Query("direction") direction: Direction?,
                 @Query("page") page: Int,
                 @Query("per_page") perPage: Int): Single<VideoWrapper>

    @GET("users/{user_id}/videos")
    fun getUsersVideos(@Path("user_id") userId: Long,
                 @Query("direction") direction: Direction?,
                 @Query("page") page: Int,
                 @Query("per_page") perPage: Int): Single<VideoWrapper>

    @GET("me/watchlater")
    fun getWatchLater(@Query("direction") direction: Direction?,
                      @Query("page") page: Int,
                      @Query("per_page") perPage: Int): Single<VideoWrapper>
}