package ru.devtron.vormeoclient.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.stepstone.apprating.listener.RatingDialogListener
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.di.DI
import ru.devtron.vormeoclient.model.system.message.SystemMessageNotifier
import ru.devtron.vormeoclient.model.system.message.SystemMessageType
import ru.devtron.vormeoclient.presentation.app.AppPresenter
import ru.devtron.vormeoclient.presentation.app.AppView
import ru.devtron.vormeoclient.presentation.global.GlobalVideoController
import ru.devtron.vormeoclient.ui.global.BaseFragment
import ru.devtron.vormeoclient.ui.video.VideoFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import toothpick.Toothpick
import javax.inject.Inject

class AppActivity : MvpAppCompatActivity(), AppView, RatingDialogListener {
    @InjectPresenter
    lateinit var presenter: AppPresenter

    @ProvidePresenter
    fun providePresenter() =
            Toothpick.openScope(DI.SERVER_SCOPE)
                    .getInstance(AppPresenter::class.java)

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var systemMessageNotifier: SystemMessageNotifier

    @Inject
    lateinit var globalVideoController: GlobalVideoController

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.container) as? BaseFragment

    private val navigator: Navigator =
            object : SupportAppNavigator(this, supportFragmentManager, R.id.container) {
                override fun createActivityIntent(
                        context: Context,
                        screenKey: String,
                        data: Any?
                ): Intent? = Screens.createIntent(screenKey, data)

                override fun exit() {
                    finish()
                }

                override fun createFragment(screenKey: String, data: Any?) =
                        Screens.createFragment(screenKey, data)

                override fun setupFragmentTransactionAnimation(
                        command: Command?,
                        currentFragment: Fragment?,
                        nextFragment: Fragment?,
                        fragmentTransaction: FragmentTransaction
                ) {
                    //fix incorrect order lifecycle callback of MainFlowFragment
                    fragmentTransaction.setReorderingAllowed(true)
                }
            }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        Toothpick.inject(this, Toothpick.openScope(DI.SERVER_SCOPE))
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.videoContainerFrame, VideoFragment())
                    .commit()

            presenter.coldStart()
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        subscribeOnSystemMessages()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        unsubscribeOnSystemMessages()
        super.onPause()
    }

    override fun onBackPressed() {
        when {
            globalVideoController.progressAnim.value == 1f -> globalVideoController.dropVideo()
            currentFragment != null -> currentFragment?.onBackPressed()
            else -> super.onBackPressed()
        }
    }

    private fun showAlertMessage(message: String) = alert(message).show()

    private fun showToastMessage(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    private fun subscribeOnSystemMessages() {
        compositeDisposable += systemMessageNotifier.notifier
                .subscribe { msg ->
                    when (msg.type) {
                        SystemMessageType.ALERT -> showAlertMessage(msg.text)
                        SystemMessageType.TOAST -> showToastMessage(msg.text)
                    }
                }
    }

    private fun unsubscribeOnSystemMessages()= compositeDisposable.dispose()

    override fun onNegativeButtonClicked() {}

    override fun onNeutralButtonClicked() {}

    override fun onPositiveButtonClicked(rate: Int, comment: String) {
        if (rate >= 4) {
            val i = Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse("market://details?id=$packageName")
            }
            startActivity(i)
        } else {
            toast(R.string.thanks_feedback)
            //todo send to analytics
        }
    }
}