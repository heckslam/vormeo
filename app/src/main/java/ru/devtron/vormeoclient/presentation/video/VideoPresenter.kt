package ru.devtron.vormeoclient.presentation.video

import android.net.Uri
import com.arellomobile.mvp.InjectViewState
import io.reactivex.rxkotlin.plusAssign
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.model.interactor.player.VideoPlayerInteractor
import ru.devtron.vormeoclient.model.system.None
import ru.devtron.vormeoclient.model.system.Some
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import ru.devtron.vormeoclient.presentation.global.ErrorHandler
import ru.devtron.vormeoclient.presentation.global.GlobalVideoController
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

/**
 * Created on 11.09.2018 by ruslanaliev.
 */
@InjectViewState
class VideoPresenter @Inject constructor(
        private val videoPlayerInteractor: VideoPlayerInteractor,
        private val router: Router,
        private val errorHandler: ErrorHandler,
        private val globalVideoController: GlobalVideoController
) : BasePresenter<VideoView>() {

    var currentVideoPosition: Long = 0
    var playerIsReadyState: Boolean = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        disposables += globalVideoController.state.subscribe {
            when (it) {
                is Some -> {
                    viewState.openVideoAnim()
                    loadVideoUrl(it.value)
                }
                is None -> viewState.closeVideoAnim()
            }
        }
    }

    private fun loadVideoUrl(video: Video) {
        val videoId = Uri.parse(video.uri).lastPathSegment
        Timber.d("videoId $videoId")
        videoId?.let { url ->
            disposables += videoPlayerInteractor.getVideoUrl(url)
                    .subscribe(
                            { viewState.showVideo(it) },
                            { errorHandler.proceed(it) { viewState.showMessage(it) } }
                    )
        }
    }

    fun onResume() {
        if (currentVideoPosition > 0) viewState.restoreVideoPosition(currentVideoPosition)
        if (playerIsReadyState) viewState.restoreVideoState(playerIsReadyState)
    }

    fun onStop(currentPosition: Long?, playWhenReady: Boolean) {
        currentVideoPosition = currentPosition ?: 0

        playerIsReadyState = playWhenReady

        viewState.pauseVideo()
    }

    override fun onDestroy() {
        viewState.stopVideo()
        super.onDestroy()
    }

    fun onTransitionChange(abs: Float) {
        globalVideoController.updateProgressAnim(abs)
    }
}