package ru.devtron.vormeoclient.entity.feed

import com.google.gson.annotations.SerializedName

data class MetaInfo(
        @SerializedName("connections") val connections: Connections,
        @SerializedName("interactions") val interactions: Interactions
)