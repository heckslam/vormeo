package ru.devtron.vormeoclient.ui.global.list

import android.content.res.Resources
import android.graphics.Bitmap
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import kotlinx.android.synthetic.main.item_feed.view.*
import ru.devtron.vormeoclient.R
import ru.devtron.vormeoclient.entity.video.Video
import ru.devtron.vormeoclient.extension.formatDuration
import ru.devtron.vormeoclient.extension.humanTime
import ru.devtron.vormeoclient.extension.humanViewCount
import ru.devtron.vormeoclient.extension.inflate
import ru.devtron.vormeoclient.ui.global.glide.GlideApp

class LikeAdapterDelegate(private val clickListener: (Video) -> Unit,
                          private val profileClickListener: (Long) -> Unit) : AdapterDelegate<MutableList<ListItem>>() {

    override fun isForViewType(items: MutableList<ListItem>, position: Int) =
            items[position] is ListItem.VideoItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
            VideoViewHolder(parent.inflate(R.layout.item_feed), clickListener, profileClickListener)

    override fun onBindViewHolder(items: MutableList<ListItem>,
                                  position: Int,
                                  viewHolder: RecyclerView.ViewHolder,
                                  payloads: MutableList<Any>) =
            (viewHolder as VideoViewHolder).bind((items[position] as ListItem.VideoItem).video)

    private class VideoViewHolder(val view: View, clickListener: (Video) -> Unit,
                                  profileClickListener: (Long) -> Unit) : RecyclerView.ViewHolder(view) {
        private lateinit var video: Video

        init {
            view.setOnClickListener { clickListener.invoke(video) }
            view.avatarIV.setOnClickListener({ profileClickListener.invoke(video.user.userId) })
        }

        fun bind(videoItem: Video) {
            this.video = videoItem

            val res = view.resources
            view.feedTitleTextView.text = videoItem.name
            view.feedDescriptionTextView.text = createMessage(videoItem, res)
            view.durationTv.text = videoItem.duration.formatDuration(videoItem.duration)

            GlideApp.with(view.context)
                    .asBitmap()
                    .load(videoItem.user.pictures?.sizes?.get(3)?.link)
                    .centerCrop()
                    .into(object : BitmapImageViewTarget(view.avatarIV) {
                        override fun setResource(resource: Bitmap?) {
                            resource?.let {
                                RoundedBitmapDrawableFactory.create(view.resources, it).run {
                                    this.isCircular = true
                                    view.avatarIV.setImageDrawable(this)
                                }
                            }
                        }
                    })

            GlideApp.with(view.context)
                    .load(videoItem.pictures.sizes[3].link)
                    .centerCrop()
                    .into(view.videoPreviewImg)
        }

        private fun createMessage(video: Video, res: Resources): String {
            return res.getString(R.string.video_description_row,
                    video.user.name,
                    video.stats.plays.humanViewCount(res),
                    video.createdTime.humanTime(res))
        }
    }
}