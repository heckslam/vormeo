package ru.devtron.vormeoclient.entity.picture

import com.google.gson.annotations.SerializedName

data class PictureMe(
        @SerializedName("type") val type: String,
        @SerializedName("link") val link: String,
        @SerializedName("sizes") val sizes: List<Size>
)