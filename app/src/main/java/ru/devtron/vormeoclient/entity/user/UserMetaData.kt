package ru.devtron.vormeoclient.entity.user

import com.google.gson.annotations.SerializedName


data class UserMetaData(
        @SerializedName("connections") val connections: UserConnections,
        @SerializedName("interactions") val interactions: UserInteractions?
)