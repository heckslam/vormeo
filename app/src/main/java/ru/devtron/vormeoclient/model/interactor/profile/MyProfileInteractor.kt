package ru.devtron.vormeoclient.model.interactor.profile

import io.reactivex.Single
import ru.devtron.vormeoclient.model.repository.auth.AuthRepository
import ru.devtron.vormeoclient.model.repository.profile.ProfileRepository
import javax.inject.Inject

class MyProfileInteractor @Inject constructor(
        private val authRepository: AuthRepository,
        private val profileRepository: ProfileRepository) {

    fun getMyProfile(): Single<MyUserInfo> =
            profileRepository
                    .getMyProfile()
                    .map { MyUserInfo(it, profileRepository.getMyServerName()) }

}