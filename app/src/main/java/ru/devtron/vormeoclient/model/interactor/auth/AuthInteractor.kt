package ru.devtron.vormeoclient.model.interactor.auth

import android.net.Uri
import io.reactivex.Completable
import ru.devtron.vormeoclient.di.qualifier.ServerPath
import ru.devtron.vormeoclient.model.repository.auth.AuthRepository
import timber.log.Timber
import java.util.*
import javax.inject.Inject


class AuthInteractor(
        private val serverPath: String,
        private val authRepository: AuthRepository,
        private val hash: String,
        private val oauthParams: OAuthParams) {

    @Inject constructor(
            @ServerPath serverPath: String,
            authRepository: AuthRepository,
            oauthParams: OAuthParams
    ) : this(
            serverPath,
            authRepository,
            UUID.randomUUID().toString(),
            oauthParams
    )

    val oauthUrl = "${serverPath}oauth/authorize?client_id=${oauthParams.appId}" +
            "&redirect_uri=${oauthParams.redirectUrl}&response_type=code&state=$hash&scope=${oauthParams.scopes}"


    fun checkOAuthRedirect(url: String) = url.indexOf(oauthParams.redirectUrl) == 0

    fun isSignedIn() = authRepository.isSignedIn
    val userId = authRepository.userId

    fun login(oauthRedirect: String) =
            Completable.defer {
                if (oauthRedirect.contains(hash)) {
                    authRepository
                            .requestOAuthToken(
                                    getQueryParameterFromUri(oauthRedirect, PARAMETER_CODE),
                                    oauthParams.redirectUrl
                            )
                            .doOnSuccess {
                                authRepository.saveAuthData(it.token, it.user.userId, serverPath)
                            }
                            .ignoreElement()
                } else {
                    Completable.error(RuntimeException("Not valid oauth hash!"))
                }
            }

    fun logout() {
        authRepository.clearAuthData()
    }

    private fun getQueryParameterFromUri(url: String, queryName: String): String {
        val uri = Uri.parse(url)
        val code = uri.getQueryParameter(queryName)
        Timber.e("%s%s", "$url code ", code)
        return code
        /*val uri = URI(url)
        val query = uri.query
        val parameters = query.split("&")

        var code = ""
        for (parameter in parameters) {
            if (parameter.startsWith(queryName)) {
                code = parameter.substring(queryName.length + 1)
                break
            }
        }
        return code*/
    }

    companion object {
        private const val PARAMETER_CODE = "code"
    }
}