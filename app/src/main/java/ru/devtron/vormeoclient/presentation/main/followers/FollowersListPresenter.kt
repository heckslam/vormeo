package ru.devtron.vormeoclient.presentation.main.followers

import com.arellomobile.mvp.InjectViewState
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.di.PrimitiveWrapper
import ru.devtron.vormeoclient.di.qualifier.FollowersState
import ru.devtron.vormeoclient.di.qualifier.UserId
import ru.devtron.vormeoclient.entity.user.AuthorizedUserMe
import ru.devtron.vormeoclient.model.interactor.profile.followers.FollowersListInteractor
import ru.devtron.vormeoclient.model.system.flow.FlowRouter
import ru.devtron.vormeoclient.presentation.global.BasePresenter
import ru.devtron.vormeoclient.presentation.global.ErrorHandler
import ru.devtron.vormeoclient.presentation.global.Paginator
import javax.inject.Inject

/**
 * Created by ruslanaliev on 12.12.2017.
 */
@InjectViewState
class FollowersListPresenter @Inject constructor(
        @FollowersState val followersState: PrimitiveWrapper<Int>,
        private val router: FlowRouter,
        private val interactor: FollowersListInteractor,
        private val errorHandler: ErrorHandler,
        @UserId userIdWrapper: PrimitiveWrapper<Long>
) : BasePresenter<FollowersView>() {
    private val userId = userIdWrapper.value

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refreshFollowerRequests()
    }

    private val paginator = Paginator(
            { interactor.getMyFollowerRequests(followersState.value, userId, it) },
            object : Paginator.ViewController<AuthorizedUserMe> {
                override fun showData(show: Boolean, data: List<AuthorizedUserMe>) {
                    viewState.showFollowers(show, data)
                }

                override fun showEmptyProgress(show: Boolean) {
                    viewState.showEmptyProgress(show)
                }

                override fun showEmptyError(show: Boolean, error: Throwable?) {
                    if (error != null) {
                        errorHandler.proceed(error) { viewState.showEmptyError(show, it) }
                    } else {
                        viewState.showEmptyError(show, null)
                    }
                }

                override fun showErrorMessage(error: Throwable) {
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }

                override fun showEmptyView(show: Boolean) {
                    viewState.showEmptyView(show)
                }

                override fun showRefreshProgress(show: Boolean) {
                    viewState.showRefreshProgress(show)
                }

                override fun showPageProgress(show: Boolean) {
                    viewState.showPageProgress(show)
                }
            }
    )

    fun onFollowerRequestClick(user: AuthorizedUserMe) = router.startFlow(Screens.USER_FLOW, user.userId)
    fun refreshFollowerRequests() = paginator.refresh()
    fun loadNextFollowerRequestsPage() = paginator.loadNewPage()

    override fun onDestroy() {
        super.onDestroy()
        paginator.release()
    }
}