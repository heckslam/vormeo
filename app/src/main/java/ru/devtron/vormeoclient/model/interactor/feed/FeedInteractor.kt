package ru.devtron.vormeoclient.model.interactor.feed

import ru.devtron.vormeoclient.model.repository.feed.FeedRepository
import javax.inject.Inject

class FeedInteractor @Inject constructor(
        private val feedRepository: FeedRepository
) {
    fun getFeeds(page: Int) = feedRepository.getFeeds(page = page)
}