package ru.devtron.vormeoclient.extension

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ru.devtron.vormeoclient.R
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace

fun Navigator.setLaunchScreen(screenKey: String, data: Any? = null) {
    applyCommands(
            arrayOf(
                    BackTo(null),
                    Replace(screenKey, data)
            )
    )
}

fun Context.color(colorRes: Int) = ContextCompat.getColor(this, colorRes)

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun Fragment.shareText(text: String?) {
    text?.let {
        startActivity(Intent.createChooser(
                Intent(Intent.ACTION_SEND).apply {
                    type = "text/plain"
                    putExtra(Intent.EXTRA_TEXT, text)
                },
                getString(R.string.share_to)
        ))
    }
}

fun ImageView.loadRoundedImage(
        url: String?,
        ctx: Context? = null
) {
    Glide.with(ctx ?: context)
            .load(url)
            .apply(RequestOptions().apply {
                placeholder(R.drawable.ic_user)
                error(R.drawable.ic_user)
            })
            .apply(RequestOptions.circleCropTransform())
            .into(this)
}

fun TextView.showTextOrHide(str: String?) {
    this.text = str
    this.visible(!str.isNullOrBlank())
}

fun Fragment.showSnackMessage(message: String) {
    view?.let {
        val snackbar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
        val messageTextView = snackbar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        messageTextView.setTextColor(Color.WHITE)
        snackbar.show()
    }
}