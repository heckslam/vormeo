package ru.devtron.vormeoclient.entity.user

/**
 * Created by ruslanaliev on 13.12.2017.
 */
class FollowersHolder (val userId: Long, val state: Int, val scope: String?)