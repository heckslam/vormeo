package ru.devtron.vormeoclient.model.system.message

enum class SystemMessageType {
    ALERT,
    TOAST
}