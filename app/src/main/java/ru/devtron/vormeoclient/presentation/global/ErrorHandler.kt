package ru.devtron.vormeoclient.presentation.global

import com.jakewharton.rxrelay2.PublishRelay
import ru.devtron.vormeoclient.Screens
import ru.devtron.vormeoclient.extension.userMessage
import ru.devtron.vormeoclient.model.data.server.ServerError
import ru.devtron.vormeoclient.model.interactor.auth.AuthInteractor
import ru.devtron.vormeoclient.model.system.ResourceManager
import ru.devtron.vormeoclient.model.system.SchedulersProvider
import ru.terrakok.cicerone.Router
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ErrorHandler @Inject constructor(
        private val router: Router,
        private val authInteractor: AuthInteractor,
        private val resourceManager: ResourceManager,
        private val schedulers: SchedulersProvider
) {

    private val authErrorRelay = PublishRelay.create<Boolean>()

    init {
        subscribeOnAuthErrors()
    }

    fun proceed(error: Throwable, messageListener: (String) -> Unit = {}) {
        Timber.e("Error: $error")
        if (error is ServerError) {
            when (error.errorCode) {
                401 -> authErrorRelay.accept(true)
                else -> messageListener(error.userMessage(resourceManager))
            }
        } else {
            messageListener(error.userMessage(resourceManager))
        }
    }

    private fun subscribeOnAuthErrors() {
        authErrorRelay
                .throttleFirst(50, TimeUnit.MILLISECONDS)
                .observeOn(schedulers.ui())
                .subscribe { logout() }
    }

    private fun logout() {
        authInteractor.logout()
        router.newRootScreen(Screens.AUTH_FLOW)
    }
}
