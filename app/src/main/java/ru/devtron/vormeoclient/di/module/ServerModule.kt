package ru.devtron.vormeoclient.di.module

import android.text.TextUtils
import com.google.gson.Gson
import okhttp3.OkHttpClient
import ru.devtron.vormeoclient.BuildConfig
import ru.devtron.vormeoclient.di.provider.ApiProvider
import ru.devtron.vormeoclient.di.provider.GsonProvider
import ru.devtron.vormeoclient.di.provider.OkHttpClientProvider
import ru.devtron.vormeoclient.di.provider.PlayerApiProvider
import ru.devtron.vormeoclient.di.qualifier.ServerPath
import ru.devtron.vormeoclient.model.data.server.VimeoApi
import ru.devtron.vormeoclient.model.data.server.VimeoPlayerApi
import ru.devtron.vormeoclient.model.interactor.auth.AuthInteractor
import ru.devtron.vormeoclient.model.interactor.auth.OAuthParams
import ru.devtron.vormeoclient.model.repository.auth.AuthRepository
import ru.devtron.vormeoclient.presentation.global.ErrorHandler
import toothpick.config.Module


class ServerModule(serverUrl: String) : Module() {
    init {
        //Network
        bind(String::class.java).withName(ServerPath::class.java).toInstance(serverUrl)
        bind(Gson::class.java).toProvider(GsonProvider::class.java).providesSingletonInScope()
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java).singletonInScope()
        bind(VimeoApi::class.java).toProvider(ApiProvider::class.java).singletonInScope()
        bind(VimeoPlayerApi::class.java).toProvider(PlayerApiProvider::class.java).singletonInScope()

        //Auth
        bind(OAuthParams::class.java).toInstance(OAuthParams(
                BuildConfig.VIMEO_CLIENT_ID,
                BuildConfig.VIMEO_CLIENT_SECRET,
                BuildConfig.VIMEO_REDIRECT_URL,
                TextUtils.join(" ", BuildConfig.VIMEO_SCOPES)
        ))
        bind(AuthRepository::class.java).singletonInScope()
        bind(AuthInteractor::class.java).singletonInScope()

        //Error handler with logout logic
        bind(ErrorHandler::class.java).singletonInScope()
    }
}